package PT07Z;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.Stack;

public class Main
{
	static Node[] graph;
	public static void main( String[] args ) {
		Scanner scanner = new Scanner( System.in );
		int nodesNo = scanner.nextInt();
		graph = new Node[nodesNo+1];
		for( int i = 1; i < nodesNo; i++ ) {
			int fromNode = scanner.nextInt();
			int toNode = scanner.nextInt();

			if( graph[fromNode] == null )
				graph[fromNode] = new Node();
			if( graph[toNode] == null )
				graph[toNode] = new Node();

			graph[fromNode].neighbours.add( graph[toNode] );
			graph[toNode].neighbours.add( graph[fromNode] );

		}
		System.out.println( getLongestPath() );

		scanner.close();
	}

	static int getLongestPath(){
		int result = 0;
		Stack<Node> stack = new Stack<>();
		stack.push( graph[1] );
		node:while(!stack.isEmpty()){
			Node tmpNode = stack.peek();
			tmpNode.isVisited = true;
			for(Node child : tmpNode.neighbours){
				if(child.isVisited == false){
					child.depth = tmpNode.depth+1;
					stack.push( child );
					continue node;
				}
			}

			stack.pop();
			for(Node child : tmpNode.neighbours){
				if(child.depth > tmpNode.depth){
					if( tmpNode.maxInnerPathLen < tmpNode.level + child.level+1 ){
						tmpNode.maxInnerPathLen = tmpNode.level + child.level+1;
					}
					tmpNode.level = tmpNode.level <= child.level ? child.level+1 : tmpNode.level;
				}
			}
			result = result > tmpNode.maxInnerPathLen ? result : tmpNode.maxInnerPathLen;
		}
		return result;
	}

	static class Node
	{
		List<Node> neighbours = new ArrayList<>();
		boolean isVisited = false;
		int level = 0;
		int depth = 0;
		int maxInnerPathLen = 0;
	}
}
