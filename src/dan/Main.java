package dan;

import java.util.*;

public class Main {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int expectedResult = scanner.nextInt();

        EquationFinder finder = new EquationFinder(expectedResult);
        for (int value = scanner.nextInt(); value > 0; ) {
            finder.addValue(new Value(value));
            value = scanner.nextInt();
        }

        for (Value equation : finder.findMatchingEquations()) {
            System.out.println(equation.toString());
        }
    }
}

class EquationFinder {

    private final long expectedResult;
    private List<Value> values = new ArrayList<>();

    EquationFinder(long result) {
        expectedResult = result;
    }

    void addValue(Value value) {
        values.add(value);
    }

    Collection<Value> findMatchingEquations() {
        Collection<Value> solutions = new ArrayList<>();
        Set<Value> workingSet = new TreeSet(values);
        solutions.addAll(extractSolutions(workingSet));

        for (int iteration = 1; iteration < values.size(); iteration++) {
            workingSet.addAll(joinAll(workingSet));
            solutions.addAll(extractSolutions(workingSet));
        }

        return solutions;
    }

    private Set<Value> joinAll(Set<Value> workingSet) {
        TreeSet<Value> joiningResult = new TreeSet<>();
        for (Iterator<Value> predecesorIterator = workingSet.iterator(); predecesorIterator.hasNext(); ) {
            Value predecesor = predecesorIterator.next();
            for (Iterator<Value> succesorIterator = workingSet.iterator(); succesorIterator.hasNext(); ) {
                Value succesor = succesorIterator.next();
                if (usesDifferentValues(predecesor.getUsedValues(), succesor.getUsedValues())) {
                    if(predecesor.getValue() <= succesor.getValue()){
                        joiningResult.add(new Operation(OperationTyp.ADD, predecesor, succesor));
                        joiningResult.add(new Operation(OperationTyp.MULTIPLY, predecesor, succesor));
                    }

                    joiningResult.add(new Operation(OperationTyp.SUBSTRACT, predecesor, succesor));
                    if (predecesor.getValue() % succesor.getValue() == 0) {
                        joiningResult.add(new Operation(OperationTyp.DIVIDE, predecesor, succesor));
                    }
                }
            }
        }
        return joiningResult;
    }

    private boolean usesDifferentValues(Set<Value> setA, Set<Value> setB) {
        Set<Value> mutalValues = new TreeSet<>(setA);
        mutalValues.retainAll(setB);
        return mutalValues.isEmpty();
    }

    private Collection<? extends Value> extractSolutions(Set<Value> workingSet) {
        Collection<Value> solutions = new ArrayList<>();

        for (Iterator<Value> iterator = workingSet.iterator(); iterator.hasNext();) {
            Value tmpValue = iterator.next();
            if(tmpValue.getValue() == 0 ){
                iterator.remove();
            }
            else if(tmpValue.getValue() == expectedResult) {
                solutions.add(tmpValue);
                iterator.remove();
            }
        }
        return solutions;
    }
}

enum OperationTyp {
    MULTIPLY('*'), DIVIDE('/'), ADD('+'), SUBSTRACT('-');

    OperationTyp(char symbol) {
        this.symbol = symbol;
    }

    char symbol;

    long perform(Value a, Value b) {
        switch (this) {
            case MULTIPLY:
                return a.getValue() * b.getValue();
            case DIVIDE:
                return a.getValue() / b.getValue();
            case ADD:
                return a.getValue() + b.getValue();
            case SUBSTRACT:
                return a.getValue() - b.getValue();
        }
        return 0;
    }
}

class Operation extends Value {
    OperationTyp type;
    private Value predecesor, succesor;

    Operation(OperationTyp type, Value predecesor, Value succesor) {
        super(type.perform(predecesor, succesor));
        this.type = type;
        this.predecesor = predecesor;
        this.succesor = succesor;
        this.usedValues.addAll(predecesor.getUsedValues());
        this.usedValues.addAll(succesor.getUsedValues());
    }

    @Override
    public String toString() {
        return "(" + predecesor.toString() + type.symbol + succesor.toString() + ")";
    }

    @Override
    public Set<Value> getUsedValues() {
        return usedValues;
    }
}

class Value implements Comparable<Value> {
    private long value;

    Set<Value> usedValues = new TreeSet<>();

    Value(long value) {
        this.value = value;
    }

    public long getValue() {
        return value;
    }

    @Override
    public String toString() {
        return Long.toString(value);
    }

    @Override
    public int compareTo(Value o) {
        return (int) (this.getValue() - o.getValue());
    }

    Set<Value> getUsedValues() {
        if (usedValues.isEmpty()) {
            usedValues.add(this);
        }
        return usedValues;
    }
}