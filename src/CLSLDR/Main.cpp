#include<stdio.h>
int leaders[1001][1001];

void init(){
        for (int counts = 2; counts <= 1000; counts++) {
            for (int students = 2; students <= 1000; students++) {
                int nextRoundStartsFrom = counts % students;
                if (students > 2) {
                    leaders[counts][students] = (leaders[counts][students - 1]+step-1)%students+1;
                } else {
                    leaders[counts][students] = nextRoundStartsFrom;
                }
            }
        }}

int main(){
	int tests, studentsNo, startingPosition, step;
	scanf("%d", &tests);
	while(tests){
		scanf("%d%d%d", &studentsNo, &startingPosition, &step);

        if(step == 1){
            printf("%d\n", startingPosition);
        }
        else{
            printf("%d\n", ((leaders[step][studentsNo] + startingPosition) % studentsNo) + 1);
        }

		tests--;
	}
	return 0;
}