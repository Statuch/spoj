#include<stdio.h>

int data[1001][1001];

int solve(int students, int step)
{
	if(students==1) return 1;
	else
	{
		data[students][step] = (solve(students-1,step)+step-1)%students+1;
		return data[students][step];
	}
}

void init()
{
	for(int i=1;i<=1000;i++)
	{
		data[1][i]=1;
		solve(1000,i);
	}
}

int main()
{
	init();
	int tests;
	scanf("%d", &tests);
	while(tests) {
		int studentsNo,startWith,step;
		scanf("%d%d%d", &studentsNo, &startWith, &step);
		printf("%d\n", (startWith+data[studentsNo][step]-1)%(studentsNo) +1);
	    tests--;
	}
}