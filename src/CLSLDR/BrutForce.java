package CLSLDR;

import java.util.LinkedList;

public class BrutForce {

    public static void main(String[] args) {
        BrutForce force = new BrutForce();

        for(int i = 1; i<10; i++){
            for(int j = 1; j<10; j++){
                System.out.print(force.whoWillWin(i, 1, j) + " ");
            }
            System.out.println();
        }
    }
    int whoWillWin(int studentsNo, int startWith, int countTo) {
        LinkedList<Integer> students = new LinkedList<>();
        for(int i = 1; i<=studentsNo;){
            students.add(i);
        }
        int currentStudentIdx = startWith-1;
        while(students.size()>1){
            currentStudentIdx+=countTo;
            currentStudentIdx%=students.size();
            students.remove(currentStudentIdx);
            currentStudentIdx--;
        }
        return students.getLast();
    }
}
