package CLSLDR;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.PrintWriter;

public class Main {
    private final static int MAX_STUDENTS = 1000;
    private final static int MAX_COUNT = 1000;
    private static PrintWriter out;
    private static int leaders[][] = new int[MAX_COUNT + 1][MAX_STUDENTS + 1];

    public static void init() {
        for (int counts = 2; counts <= MAX_COUNT; counts++) {
            for (int students = 2; students <= MAX_STUDENTS; students++) {
                int nextRoundStartsFrom = counts % students;
                if (students > 2) {
                    leaders[counts][students] = (nextRoundStartsFrom + leaders[counts][students - 1]) % (students);
                } else {
                    leaders[counts][students] = nextRoundStartsFrom;
                }
            }
        }
    }

    public static void main(String[] args) throws IOException {
        init();
        out = new PrintWriter(new BufferedOutputStream(System.out), true);
        for (int tests = readInt(); tests > 0; tests--) {
            int n = readInt();
            int m = readInt();
            int o = readInt();
            if(o == 1){
                out.println(m);
            }
            else{
                out.println(((leaders[o][n] + m) % n) + 1);
            }

        }
    }

    private static int readInt() throws IOException {
        int ret = 0;
        boolean dig = false;
        for (int c = 0; (c = System.in.read()) != -1; ) {
            if (c >= '0' && c <= '9') {
                dig = true;
                ret = ret * 10 + c - '0';
            } else if (dig) break;
        }
        return ret;
    }
}
