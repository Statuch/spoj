package solved.ATOMS;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.StringTokenizer;

public class Main {
	public static void main(String[] args) {
		MyScanner scanner = new MyScanner();
		out = new PrintWriter( System.out );
		for(long P = scanner.nextLong(); P>0; P--) {
			long N = scanner.nextLong();
			long K = scanner.nextLong();
			long M = scanner.nextLong();
			
			long result = 0;
			while(N <= M/K) {
				M/=K;
				result++;
			}
			System.out.println(result);
		}
	}
	
	// -----------PrintWriter for faster output---------------------------------
	public static PrintWriter out;

	// -----------MyScanner class for faster input----------
	public static class MyScanner {
		BufferedReader br;
		StringTokenizer st;

		public MyScanner() {
			br = new BufferedReader(new InputStreamReader(System.in));
		}

		String next() {
			while (st == null || !st.hasMoreElements()) {
				try {
					st = new StringTokenizer(br.readLine());
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			return st.nextToken();
		}

		long nextLong() {
			return Long.parseLong(next());
		}
	}
	// --------------------------------------------------------
}
