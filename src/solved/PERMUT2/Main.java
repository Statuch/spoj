package solved.PERMUT2;

import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int numbers = scanner.nextInt();
		while (numbers > 0) {
			int[] values = new int[numbers];
			for (int i = 0; i < numbers; i++) {
				values[i] = scanner.nextInt();
			}
			boolean isAmbigues = true;
			for (int i = 0; i < numbers; i++) {
				if (values[values[i]-1] != i + 1) {
					isAmbigues = false;
					break;
				}
			}
			System.out.println(isAmbigues ? "ambiguous" : "not ambiguous");
			numbers = scanner.nextInt();
		}
		scanner.close();
	}
}
