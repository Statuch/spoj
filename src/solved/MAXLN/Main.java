package solved.MAXLN;

import java.util.Scanner;

public class Main
{
	public static void main( String[] args ) {
		Scanner scanner = new Scanner(System.in);
		for(int caseNo = 1, T = scanner.nextInt(); caseNo<=T; caseNo++){
			long result = scanner.nextLong();
			result *= 4*result;
			System.out.println("Case "+caseNo+": "+result + ".25");
		}
		scanner.close();
	}
}
