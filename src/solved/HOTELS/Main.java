package solved.HOTELS;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class Main
{
	public static void main( String[] args ) {

		MyScanner scanner = new MyScanner( );
		int[] hotelPrices = new int[300001];

		int sum = 0;
		int result = 0;
		int startIdx = -1;
		int endIdx = -1;
		for( int N = scanner.nextInt(), award = scanner.nextInt(); N > 0; N-- ) {

			int tmpPrice = scanner.nextInt();
			hotelPrices[++endIdx] = tmpPrice;
			sum += tmpPrice;
			while( sum > award ) {
				sum -= hotelPrices[++startIdx];
			}
			if( sum > result && sum <= award ) {
				result = sum;
			}

		}
		System.out.println( result );
	}

	public static class MyScanner {
		BufferedReader br;
		StringTokenizer st;

		public MyScanner() {
			br = new BufferedReader(new InputStreamReader(System.in));
		}

		String next() {
			while (st == null || !st.hasMoreElements()) {
				try {
					st = new StringTokenizer(br.readLine());
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			return st.nextToken();
		}
		int nextInt() {
			return Integer.parseInt(next());
		}
	}
}
