package solved.MAJOR;

import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException{

        testcase: for(int testCases = readInt(); testCases>0; testCases--){


            int numbers = readInt();
            int[] numbersArray = new int[numbers];

            int candidate = numbersArray [0] = readInt();
            int count = 1;

            for(int i =1; i<numbers; i++){
                int tmpInt = readInt();
                numbersArray[i] = tmpInt;

                    count += tmpInt == candidate ? 1 : -1;
                    if(count < 0){
                        candidate = tmpInt;
                        count=1;
                    }
                }

            if(count>0){
                count = 0;
                for(int i : numbersArray){
                    count+=candidate==i?1:0;
                }
                if(count*2>numbers){
                    System.out.println("YES "+candidate);
                    continue testcase;
                }
            }
            System.out.println("NO");

        }
    }

    private static int readInt() throws IOException {
        int ret = 0;
        boolean dig = false;
        for (int c = 0; (c = System.in.read()) != -1; ) {
            if (c >= '0' && c <= '9') {
                dig = true;
                ret = ret * 10 + c - '0';
            } else if (dig) break;
        }
        return ret;
    }

}
