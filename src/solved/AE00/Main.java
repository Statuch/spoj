package solved.AE00;

import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int i = scanner.nextInt();
		int result = i;
		for (int n = 2; n * n <= i; n++) {
			result += ((i - (i % n)) / n) - (n - 1);
		}
		System.out.println(result);
		scanner.close();
	}
}
