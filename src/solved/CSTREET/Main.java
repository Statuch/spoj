package solved.CSTREET;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        for (int tests = scanner.nextInt(); tests > 0; tests--) {
            int cost = scanner.nextInt();
            DisjointSet<Integer>[] cities = new DisjointSet[scanner.nextInt()+1];
            List<Path> paths = new ArrayList<>();
            for (int pathsNo = scanner.nextInt(); pathsNo > 0; pathsNo--) {
                paths.add(new Path(scanner.nextInt(), scanner.nextInt(), scanner.nextInt()));
            }
            paths.sort(Path::compareTo);
            int totalDistance = 0;
            for (Path path : paths) {
                DisjointSet<Integer> from = cities[path.from];
                if (from == null) {
                    from = new DisjointSet<>(path.from);
                    cities[path.from] = from;
                }

                DisjointSet<Integer> to = cities[path.to];
                if (to == null) {
                    to = new DisjointSet<>(path.to);
                    cities[path.to] = to;
                }
                if (from.join(to)) {
                    totalDistance += path.distance;
                }
            }
            System.out.println(totalDistance * cost);
        }
        scanner.close();
    }

}

class Path implements Comparable<Path> {

    final int from, to, distance;

    public Path(int from, int to, int distance) {
        this.from = from;
        this.to = to;
        this.distance = distance;
    }

    @Override
    public int compareTo(Path o) {
        return distance - o.distance;
    }
}

class DisjointSet<T> {
    T value;
    DisjointSet<T> parrent = null;

    DisjointSet(T value) {
        this.value = value;
    }

    boolean join(DisjointSet<T> set) {
        if (getRepresentative().equals(set.getRepresentative())) {
            return false;
        } else {
            set.getRepresentative().parrent = this;
            return true;
        }
    }

    DisjointSet<T> getRepresentative() {
        return parrent == null ? this : parrent.getRepresentative();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof DisjointSet)
            return value.equals(((DisjointSet) obj).value);
        return false;
    }
}
