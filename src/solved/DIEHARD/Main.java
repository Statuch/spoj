package solved.DIEHARD;

import java.util.Scanner;

public class Main {
    private final static int AIR_HEALTH = 3, AIR_ARMOR = 2,
            WATER_HEALTH = -5, WATER_ARMOR = -10,
            FIRE_HEALTH = -20, FIRE_ARMOR = 5;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        for (int i = 1, tests = scanner.nextInt(); i <= tests; i++) {
            System.out.println(survive(scanner.nextInt(), scanner.nextInt()));
        }
        scanner.close();
    }

    private static int survive(int health, int armor) {

        int airWatherHealth = AIR_HEALTH + WATER_HEALTH;
        int airWatherArmor = AIR_ARMOR + WATER_ARMOR;

        int airFireHealth = AIR_HEALTH + FIRE_HEALTH;
        int airFireArmor = AIR_ARMOR + FIRE_ARMOR;

        int bestScore = 0;
        int tempScore = 0;

        for (int inFire = 0; true; inFire++) {
            if ( health <= inFire * (-airFireHealth)) {
                tempScore = (2 * inFire - 1);
                bestScore = bestScore > tempScore ? bestScore : tempScore;
                break;
            }

            tempScore = inFire;
            int remHealth = health + (inFire * airFireHealth);
            int remArmor = armor + (inFire * airFireArmor);

            if ( (remArmor - 1) / (-airWatherArmor) + 1 > (remHealth - 1) / (-airWatherHealth) + 1) {
                tempScore += (remHealth - 1) / (-airWatherHealth) + 1;
            }
            else{
                tempScore += (remArmor - 1) / (-airWatherArmor) + 1;
            }

            tempScore = (2 * tempScore - 1);
            bestScore = bestScore > tempScore ? bestScore : tempScore;
        }
        return bestScore;

    }
}
