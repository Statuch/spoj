package solved.JASIEK;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import java.util.function.UnaryOperator;

public class Main {

    static final UnaryOperator<Point> BELOW = (point) -> new Point(point.x + 1, point.y - 1);
    static final UnaryOperator<Point> RIGHT = (point) -> new Point(point.x + 1, point.y + 1);
    static final UnaryOperator<Point> ABOVE = (point) -> new Point(point.x - 1, point.y + 1);
    static final UnaryOperator<Point> LEFT = (point) -> new Point(point.x - 1, point.y - 1);

    static List<Point> getPoints(Point startingPoint, UnaryOperator<Point>... actions) {
        var lastPoint = startingPoint;
        LinkedList<Point> result = new LinkedList<>();
        for (var action : actions) {
            lastPoint = action.apply(lastPoint);
            result.add(lastPoint);
        }
        return result;
    }

    static class Point {
        int x, y;

        Point(int x, int y) {
            this.x = x;
            this.y = y;
        }
    }

    enum Direction {
        P, N {
            List<Point> get(Direction prev, Point point) {
                switch (prev) {
                    case P:
                    case N:
                        return getPoints(point, ABOVE);
                    case E:
                        return getPoints(point, RIGHT, ABOVE);
                    case S:
                        return getPoints(point, BELOW, RIGHT, ABOVE);
                    case W:
                    default:
                        return Collections.emptyList();
                }

            }
        }, E {
            List<Point> get(Direction prev, Point point) {
                switch (prev) {
                    case P:
                    case E:
                        return getPoints(point, RIGHT);
                    case S:
                        return getPoints(point, BELOW, RIGHT);
                    case W:
                        return getPoints(point, LEFT, BELOW, RIGHT);
                    case N:
                    default:
                        return Collections.emptyList();
                }
            }
        }, S {
            List<Point> get(Direction prev, Point point) {
                switch (prev) {
                    case P:
                    case S:
                        return getPoints(point, BELOW);
                    case W:
                        return getPoints(point, LEFT, BELOW);
                    case N:
                        return getPoints(point, ABOVE, LEFT, BELOW);
                    case E:
                    default:
                        return Collections.emptyList();
                }
            }
        }, W {
            List<Point> get(Direction prev, Point point) {
                switch (prev) {
                    case P:
                    case W:
                        return getPoints(point, LEFT);
                    case N:
                        return getPoints(point, ABOVE, LEFT);
                    case E:
                        return getPoints(point, RIGHT, ABOVE, LEFT);
                    case S:
                    default:
                        return Collections.emptyList();
                }
            }
        }, K;

        List<Point> get(Direction prev, Point point) {
            return Collections.emptyList();
        }

    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        for (int count = 0; count < 10; count++) {
            Direction prevDirection = Direction.valueOf(scanner.nextLine());

            LinkedList<Point> points = new LinkedList<>();
            points.add(new Point(0, 0));

            for (Direction currentDirection = Direction.valueOf(scanner.nextLine());
                 currentDirection != Direction.K;
                 prevDirection = currentDirection, currentDirection = Direction.valueOf(scanner.nextLine())) {
                points.addAll(currentDirection.get(prevDirection, points.getLast()));
            }
            int total = 0;

            var iterator = points.iterator();
            for (var prevPoint = iterator.next(); iterator.hasNext(); ) {
                var currentPoint = iterator.next();
                total += prevPoint.y * currentPoint.x - prevPoint.x * currentPoint.y;
                prevPoint = currentPoint;
            }
            total = total > 0 ? total : -total;
            System.out.println(total > 0 ? (total + 2) / 4 : 1);
        }

    }

}
