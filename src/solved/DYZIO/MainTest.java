package solved.DYZIO;

import org.junit.Assert;
import org.junit.Test;

public class MainTest {

    @Test
    public void test(){
        Main main = new Main();

        Assert.assertEquals(4, main.getFirstDeepestCut("110011000".toCharArray()));
        Assert.assertEquals(3, main.getFirstDeepestCut("111000100".toCharArray()));
        Assert.assertEquals(0, main.getFirstDeepestCut("010".toCharArray()));
        Assert.assertEquals(1, main.getFirstDeepestCut("1".toCharArray()));
        Assert.assertEquals(2, main.getFirstDeepestCut("11".toCharArray()));
        Assert.assertEquals(7, main.getFirstDeepestCut("111100111000000".toCharArray()));
        Assert.assertEquals(7, main.getFirstDeepestCut("101010110011000".toCharArray()));
        Assert.assertEquals(9, main.getFirstDeepestCut("1100111011001010000".toCharArray()));
        Assert.assertEquals(5, main.getFirstDeepestCut("1100111000100".toCharArray()));
        Assert.assertEquals(4, main.getFirstDeepestCut("111010001010100".toCharArray()));
    }
}