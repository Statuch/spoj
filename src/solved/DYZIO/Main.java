package solved.DYZIO;

import java.util.LinkedList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        for (int count = 0; count < 10; count++) {
            scanner.nextLine();
            System.out.println(getFirstDeepestCut(scanner.nextLine().toCharArray()));
        }
    }

    static int getFirstDeepestCut(char[] cutsSequence) {
        LinkedList<Character> lineCutting = new LinkedList<>();
        char prevCut = ' ';
        int cutCount = 0;
        int deepestCut = 0;
        int firstDeepestCut = 0;

        for (char cut : cutsSequence) {
            if (cut == '0') {
                if (lineCutting.isEmpty()) break;
                var last = lineCutting.pollLast();
                while(last == 'R' && !lineCutting.isEmpty()){
                    last = lineCutting.pollLast();
                }
                if (last == 'L') {
                    lineCutting.add('R');
                }
            } else {
                cutCount++;
                lineCutting.add('L');
                if (deepestCut < lineCutting.size()) {
                    //new best cut
                    firstDeepestCut = cutCount;
                    deepestCut = lineCutting.size();
                }

            }
            prevCut = cut;
        }
        return firstDeepestCut;
    }
}
