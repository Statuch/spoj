package solved.TOANDFRO;

import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
		Scanner reader = new Scanner(System.in);
		int columns = Integer.parseInt(reader.nextLine());
		while (columns > 0) {
			char[] input = reader.nextLine().toCharArray();
			int rows = input.length / columns;
			char[][] tab = new char[rows][columns];
			char[] output = new char[input.length];

			for (int i = 0; i < rows; i++) {
				for (int j = 0; j < columns; j++) {
					if (i % 2 == 0) {
						tab[i][j] = input[(i * columns) + j];
					} else {
						tab[i][(columns-1) - j] = input[(i * columns) + j];
					}
				}
			}
			for(int i = 0; i<columns; i++) {
				for(int j = 0; j<rows; j++) {
					output[(i*rows)+j] = tab[j][i];
				}
			}
			
			System.out.println(output);
			columns = Integer.parseInt(reader.nextLine());
		}
		reader.close();
	}
}
