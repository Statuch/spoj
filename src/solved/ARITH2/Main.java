package solved.ARITH2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class Main {
	public static void main(String[] args) {
		MyScanner scanner = new MyScanner();
		for(int test = scanner.nextInt(); test>0; test--) {
			scanner.nextLine();
			long result = 0;
			char operator = '+';
			do {
				long tmpValue = scanner.nextLong();
				result = calculate(result, tmpValue, operator);
				operator = scanner.next().charAt(0);
			}while(operator != '=');
			System.out.println(result);
		}
	}

	
	private static long calculate(long result, long tmpValue, char operator) {
		switch(operator) {
		case '*':
			return result*tmpValue;
		case '/':
			return result/tmpValue;
		case '+':
			return result+tmpValue;
		default:
			return result-tmpValue;
		}
	}


	public static class MyScanner {
		BufferedReader br;
		StringTokenizer st;

		public MyScanner() {
			br = new BufferedReader(new InputStreamReader(System.in));
		}

		String next() {
			while (st == null || !st.hasMoreElements()) {
				try {
					st = new StringTokenizer(br.readLine());
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			return st.nextToken();
		}

		int nextInt() {
			return Integer.parseInt(next());
		}

		long nextLong() {
			return Long.parseLong(next());
		}

		double nextDouble() {
			return Double.parseDouble(next());
		}

		String nextLine() {
			String str = "";
			try {
				str = br.readLine();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return str;
		}
	}

}
