package solved.AIBOHP;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int tests = scanner.nextInt();
        for (scanner.nextLine(); tests > 0; tests--) {
            String word = scanner.nextLine();
            System.out.println(word.length()-lcsLenth(word.toCharArray(), palindrome(word) ));
        }
        scanner.close();
    }
    private static char[] palindrome(String word){
        char[] polindrome = word.toCharArray();
        for(int i =0, lenght = polindrome.length, last = polindrome.length/2; i<last; i++){
            char tmpChar = polindrome[i];
            polindrome[i]= polindrome[polindrome.length-1-i];
            polindrome[polindrome.length-1-i] = tmpChar;
        }
        return  polindrome;
    }

    private static int lcsLenth(char[] wordA, char[] wordB){
        int [] previousRow = new int[wordB.length+1];
        int [] currentRow = new int[wordB.length+1];
        for(char a : wordA){
            for(int i = 1, len=currentRow.length; i<len; i++){
                if(a == wordB[i-1]){
                    currentRow[i] = previousRow[i-1]+1;
                }
                else{
                    currentRow[i] = previousRow[i]>currentRow[i-1] ? previousRow[i] : currentRow[i-1];
                }
            }
            int [] rowSwop = previousRow;
            previousRow = currentRow;
            currentRow = rowSwop;
        }
        return previousRow[previousRow.length-1];
    }
}
