package solved.CANTON;

import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		for (int tests = scanner.nextInt(); tests > 0; tests--) {
			long term = scanner.nextLong();
			long row = (long)(Math.sqrt(1+(8*(term-1))) + 1 ) / 2;
			long first = ((row*row)-row)/2 + 1;
			long offset = term - first; 
			if (row % 2 == 0) {
				System.out.println("TERM "+ term +" IS "+(offset+1) + "/" + (row - offset));				
			} else {
				System.out.println("TERM "+ term +" IS "+(row - offset) + "/" + (offset+1));
			}
		}
		scanner.close();
	}
}
