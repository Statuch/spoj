package solved.ARMY;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class Main
{
	public static void main( String[] args ) {
		MyScanner scanner = new MyScanner();
		for(int tests = scanner.nextInt(); tests>0; tests--){
			scanner.nextLine();
			int ng = scanner.nextInt();
			int bestG = 0;
			int nmg = scanner.nextInt();
			int bestMegaG = 0;
			for(int i = 0; i<ng; i++ ){
				int strength = scanner.nextInt();
				bestG = bestG > strength ? bestG : strength;
			}
			for(int i = 0; i<nmg; i++ ){
				int strength = scanner.nextInt();
				bestMegaG = bestMegaG > strength ? bestMegaG : strength;
			}
				System.out.println(bestG >= bestMegaG ? "Godzilla" : "MechaGodzilla");


		}
		//scanner.close();
	}
	public static class MyScanner {
		BufferedReader br;
		StringTokenizer st;

		public MyScanner() {
			br = new BufferedReader(new InputStreamReader(System.in));
		}

		String next() {
			while (st == null || !st.hasMoreElements()) {
				try {
					st = new StringTokenizer(br.readLine());
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			return st.nextToken();
		}

		int nextInt() {
			return Integer.parseInt(next());
		}

		long nextLong() {
			return Long.parseLong(next());
		}

		double nextDouble() {
			return Double.parseDouble(next());
		}

		String nextLine() {
			String str = "";
			try {
				str = br.readLine();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return str;
		}
	}
}
