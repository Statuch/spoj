package solved.STRHH;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.StringTokenizer;

public class Main {
	private static PrintWriter out = new PrintWriter(new BufferedOutputStream(System.out), true);

	public static void main(String[] args) {
		MyScanner scanner = new MyScanner();
		StringBuffer buffer = new StringBuffer();
		for (int lines = scanner.nextInt(); lines > 0; lines--) {
			char[] signs = scanner.nextLine().toCharArray();
			for (int i = 0, max = signs.length / 2; i+1 <= max; i += 2) {
				buffer.append(signs[i]);
			}
			buffer.append("\n");
		}
		out.println(buffer.toString());

	}

	public static class MyScanner {
		BufferedReader br;
		StringTokenizer st;

		public MyScanner() {
			br = new BufferedReader(new InputStreamReader(System.in));
		}

		String next() {
			while (st == null || !st.hasMoreElements()) {
				try {
					st = new StringTokenizer(br.readLine());
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			return st.nextToken();
		}

		int nextInt() {
			return Integer.parseInt(next());
		}

		long nextLong() {
			return Long.parseLong(next());
		}

		double nextDouble() {
			return Double.parseDouble(next());
		}

		String nextLine() {
			String str = "";
			try {
				str = br.readLine();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return str;
		}
	}
}
