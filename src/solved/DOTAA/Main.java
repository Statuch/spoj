package solved.DOTAA;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.StringTokenizer;

public class Main {
	public static PrintWriter out = new PrintWriter(new BufferedOutputStream(System.out), true);

	public static void main(String[] args) {

		MyScanner scanner = new MyScanner();
		for (int tests = scanner.nextInt(); tests > 0; tests--) {
			int knightsNo = scanner.nextInt();
			int towersNo = scanner.nextInt();
			int damage = scanner.nextInt();

			int canReceiveHits = 0;
			for (int i = 1; i <= knightsNo; i++) {
				int knightHp = scanner.nextInt();
				canReceiveHits += (knightHp - 1) / damage;
			}
			out.println(canReceiveHits >= towersNo ? "YES" : "NO");
		}
		out.close();
	}

	public static class MyScanner {
		BufferedReader br;
		StringTokenizer st;

		public MyScanner() {
			br = new BufferedReader(new InputStreamReader(System.in));
		}

		String next() {
			while (st == null || !st.hasMoreElements()) {
				try {
					st = new StringTokenizer(br.readLine());
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			return st.nextToken();
		}

		int nextInt() {
			return Integer.parseInt(next());
		}
	}
}