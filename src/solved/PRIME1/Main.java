package solved.PRIME1;

import java.util.*;
import java.lang.*;

public class Main {

	public static void main(String[] args) {
		Scanner scn = new Scanner(System.in);
		for (int i = 0, t = scn.nextInt(); i < t; i++) {
			int start = scn.nextInt();
			start = start==1? 2: start;

			int end = scn.nextInt();
			numbers:for (;start <= end; start++) {
				for (int j = 2, sqrt = (int) Math.sqrt(start); j <= sqrt; j++) {
					if(start%j== 0) {
						continue numbers;
					}
				}
				System.out.println(start);
			}
			System.out.println();
		}
	}
}
