#include <stdio.h>
using namespace std;

int main()
{
    long val;
    long result =0;
    int len = 0;
    scanf("%d", &len);
    for (int i=0; i<len; i++)
    {
    	scanf("%ld", &val);
        result ^= val;
    }
    printf( "%ld" ,result);
    return 0;
}
