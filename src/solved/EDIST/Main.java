package solved.EDIST;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class Main {
    public static void main(String[] args) {
        MyScanner scanner = new MyScanner();
        int tests = Integer.parseInt(scanner.nextLine());
        for (; tests > 0; tests--) {
            System.out.println(minEdit(scanner.nextLine(), scanner.nextLine()));
        }
    }

    private static int minEdit(String first, String second) {
        char[] firstChArr = first.toCharArray();
        char[] secondChArr = second.toCharArray();

        int[] minEditCost = new int[secondChArr.length + 1];
        for (int i = 0, last = secondChArr.length; i <= last; i++) minEditCost[i] = i;

        int previous = 0;
        int val = 0;
        for (char charA : firstChArr) {
            int tmpPrev = ++previous;
            for (int i = 1, size = secondChArr.length; i <= size; i++) {
                char charB = secondChArr[i - 1];
                if (charA == charB) {
                    val = minEditCost[i - 1];
                } else {
                    val = min(minEditCost[i], minEditCost[i - 1], tmpPrev) + 1;
                }
                minEditCost[i - 1] = tmpPrev;
                tmpPrev = val;
            }
            minEditCost[minEditCost.length - 1] = val;
        }
        return minEditCost[minEditCost.length - 1];
    }

    private static int min(int... values) {
        int min = Integer.MAX_VALUE;
        for (int val : values) {
            min = min < val ? min : val;
        }
        return min;
    }

    public static class MyScanner {
        BufferedReader br;
        StringTokenizer st;

        public MyScanner() {
            br = new BufferedReader(new InputStreamReader(System.in));
        }

        String next() {
            while (st == null || !st.hasMoreElements()) {
                try {
                    st = new StringTokenizer(br.readLine());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return st.nextToken();
        }

        int nextInt() {
            return Integer.parseInt(next());
        }

        String nextLine() {
            String str = "";
            try {
                str = br.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return str;
        }
    }
}

