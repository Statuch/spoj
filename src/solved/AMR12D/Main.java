package solved.AMR12D;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int tests = scanner.nextInt();
        scanner.nextLine();
        for(int i = 1; i<=tests; i++){
            String string = scanner.nextLine();
            System.out.println(isMagical(string)? "YES" : "NO");
        }
    }

    private static boolean isMagical(String word) {
        char[] wordChars = word.toCharArray();
        for(int start = 0, end = wordChars.length-1; start<end; start++, end--){
            if(wordChars[start]!=wordChars[end]) return false;
        }
        return true;
    }
}
