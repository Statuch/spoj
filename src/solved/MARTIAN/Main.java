package solved.MARTIAN;

import java.io.IOException;

import static java.lang.StrictMath.max;

public class Main {
    public static void main(String[] args) throws IOException {
        int areaLength = readInt(), areaWidth = readInt();
        while (areaLength > 0) {
            System.out.println(
                    calculateMaxResourceYield(
                            areaLength,
                            areaWidth,
                            readYeyenumResouceAvailability(areaLength, areaWidth),
                            readBloggiumResouceAvailability(areaLength, areaWidth)));
            areaLength = readInt();
            areaWidth = readInt();
        }
    }

    private static int calculateMaxResourceYield(int length, int width, int[][] yeyenumResourcesMap, int[][] bloggiumResourcesMap) {
        int[][] scoreMatrix = new int[length + 1][width + 1];

        for (int row = 1; row <= length; row++) {
            for (int column = 1; column <= width; column++) {

                scoreMatrix[row][column] = max(
                        bloggiumResourcesMap[row][column] + scoreMatrix[row][column - 1],
                        yeyenumResourcesMap[row][column] + scoreMatrix[row - 1][column]);
            }
        }
        return scoreMatrix[length][width];
    }

    private static int[][] readYeyenumResouceAvailability(int areaLength, int areaWidth) throws IOException {
        int[][] resourceMap = new int[areaLength + 1][areaLength + 1];
        for (int row = 1; row <= areaLength; row++) {
            for (int column = 1; column <= areaWidth; column++) {
                resourceMap[row][column] = readInt() + resourceMap[row][column - 1];
            }
        }
        return resourceMap;
    }

    private static int[][] readBloggiumResouceAvailability(int areaLength, int areaWidth) throws IOException {
        int[][] resourceMap = new int[areaLength + 1][areaLength + 1];
        for (int row = 1; row <= areaLength; row++) {
            for (int column = 1; column <= areaWidth; column++) {
                resourceMap[row][column] = readInt() + resourceMap[row - 1][column];
            }
        }
        return resourceMap;
    }

    private static int readInt() throws IOException {
        int ret = 0;
        boolean dig = false;
        for (int c = 0; (c = System.in.read()) != -1; ) {
            if (c >= '0' && c <= '9') {
                dig = true;
                ret = ret * 10 + c - '0';
            } else if (dig) break;
        }
        return ret;
    }
}