package solved.AP2AP;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.StringTokenizer;

public class Main {
	public static PrintWriter out;

	public static void main(String[] args) {
		out = new PrintWriter(new BufferedOutputStream(System.out), true);
		MyScanner scanner = new MyScanner();
		for (long tests = scanner.nextLong(); tests > 0; tests--) {
			long third = scanner.nextLong();
			long thirdLast = scanner.nextLong();
			long sum = scanner.nextLong();

			long elements = sum / (third + thirdLast) * 2;
			if (sum % (third + thirdLast) != 0) {
				elements++;
			}

			long gap = (thirdLast - third) / (elements - 5);
			long first = third - (2 * gap);
			out.println(elements);
			for (int i = 0; i < elements; i++) {
				out.print((first + (i * gap)) + " ");
			}
		}
		out.close();
	}

	public static class MyScanner {
		BufferedReader br;
		StringTokenizer st;

		public MyScanner() {
			br = new BufferedReader(new InputStreamReader(System.in));
		}

		String next() {
			while (st == null || !st.hasMoreElements()) {
				try {
					st = new StringTokenizer(br.readLine());
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			return st.nextToken();
		}

		long nextLong() {
			return Long.parseLong(next());
		}
	}
}
