package solved.FENCE1;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.Scanner;

public class Main
{
	private final static double PI = 3.14159;
	public static void main( String[] args ) {

		DecimalFormat format = new DecimalFormat( "#0.00");
		format.setRoundingMode( RoundingMode.HALF_EVEN);
		Scanner scanner = new Scanner(System.in);
		for(int length = scanner.nextInt(); length > 0; length = scanner.nextInt())
		{
			System.out.println( format.format( (length*length)/(2*PI) ) );
		}
		scanner.close();
	}
}
