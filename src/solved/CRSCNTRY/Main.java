package solved.CRSCNTRY;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class Main {
    static MyScanner scanner = new MyScanner();

    public static void main(String[] args) {
        testCase: for (int testCases = scanner.readLineIntValue(); testCases > 0; testCases--) {
            int[] agnessPath = scanner.nextIntArray();
            int bestScore = 0;
            while (true) {
               int[] contenderPath = scanner.nextIntArray();
               if(contenderPath == null){
                   System.out.println(bestScore);
                   continue testCase;
               }
               else{
                   int tmpBest = lcs(agnessPath, contenderPath);
                   bestScore = bestScore> tmpBest ? bestScore : tmpBest;
               }
            }
        }
    }

    private static int lcs(int[] agnessPath, int[] contentderPath) {
        int agnessPathLen = agnessPath.length;
        int contenderPathLen = contentderPath.length;
        int[] scores = new int[contenderPathLen+1];
        int[] previous = new int[contenderPathLen+1];

        for(int aggnessStop: agnessPath){
            for(int j = 1; j<=contenderPathLen;j++){
                if(aggnessStop == contentderPath[j-1])scores[j] = previous[j-1] +1;
                else{
                    scores[j] = previous[j] > scores[j-1] ? previous[j] : scores[j-1];
                }
            }
            int[] tmp = previous;
            previous=scores;
            scores = tmp;
        }
        return previous[contenderPathLen];
    }

    public static class MyScanner {
        BufferedReader br;
        StringTokenizer st;

        public MyScanner() {
            br = new BufferedReader(new InputStreamReader(System.in));
        }

        void nextLine() {
            try {
                st = new StringTokenizer(br.readLine());
            } catch (IOException exc) {
            }
        }

        boolean hasNext() {
            return st.hasMoreElements();
        }

        int[] nextIntArray() {
            nextLine();
            int [] tokens = null;
            int tokensNo = st.countTokens()-1;
            if(tokensNo > 0 ){
                tokens = new int[tokensNo];
                for(int i = 0; i<tokensNo; i++){
                    tokens[i] = Integer.parseInt(st.nextToken());
                }
            }
            return tokens;
        }

        int readLineIntValue(){
            nextLine();
            return Integer.parseInt(st.nextToken());
        }
    }
}
