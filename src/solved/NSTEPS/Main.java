package solved.NSTEPS;

import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		for (int tests = scanner.nextInt(); tests > 0; tests--) {
			solve(scanner.nextInt(), scanner.nextInt());
		}
		scanner.close();
	}

	private static void solve(int x, int y) {
		if (x == y || x - 2 == y) {
			System.out.println(x + y - (x % 2));
		} else {
			System.out.println("No Number");
		}
	}
}
