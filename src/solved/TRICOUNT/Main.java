package solved.TRICOUNT;

import java.io.BufferedOutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scn = new Scanner(System.in);
        PrintWriter out = new PrintWriter(new BufferedOutputStream(System.out), true);
        for (long tst = scn.nextLong(); tst > 0; tst--) {
            long lvl = scn.nextLong();
            long result = lvl * lvl;
            result += lvl > 1 ? ((((lvl / 2) * (lvl / 2)) + (lvl / 2)) / 2) - 1 : 0;
            long tmp = 0;
            for (int i = 1; i < lvl; i++) {
                tmp += i;
                result += tmp;
            }
            out.println(result);
        }
        out.close();
        scn.close();
    }
}