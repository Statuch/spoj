package solved.BITMAP;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.StringTokenizer;

public class Main {
	public static void main(String[] args) {
		MyScanner scanner = new MyScanner();

		for (int tests = scanner.nextInt(); tests > 0; tests--) {
			int m = scanner.nextInt();
			int n = scanner.nextInt();

			LinkedList<Point> points = new LinkedList<>();
			int[][] distanceTable = new int[m][n];
			for (int i = 0; i < m; i++) {
				char[] line = scanner.next().toCharArray();
				for (int j = 0; j < n; j++) {
					if (line[j] == '1') {
						distanceTable[i][j] = 0;
						points.add(new Point(i, j));
					} else {
						distanceTable[i][j] = -1;
					}
				}
			}

			while (!points.isEmpty()) {
				Point point = points.removeFirst();
				int pointValue = distanceTable[point.x][point.y] + 1;
				int tmpX = point.x;
				int tmpY = point.y;
				if (tmpX > 0 && distanceTable[tmpX - 1][tmpY] == -1) {
					points.addLast(new Point(tmpX - 1, tmpY));
					distanceTable[tmpX - 1][tmpY] = pointValue;
				}
				if (tmpX < m - 1 && distanceTable[tmpX + 1][tmpY] == -1) {
					points.addLast(new Point(tmpX + 1, tmpY));
					distanceTable[tmpX + 1][tmpY] = pointValue;
				}
				if (tmpY > 0 && distanceTable[tmpX][tmpY - 1] == -1) {
					points.addLast(new Point(tmpX, tmpY - 1));
					distanceTable[tmpX][tmpY - 1] = pointValue;
				}
				if (tmpY < n - 1 && distanceTable[tmpX][tmpY + 1] == -1) {
					points.addLast(new Point(tmpX, tmpY + 1));
					distanceTable[tmpX][tmpY + 1] = pointValue;
				}
			}

			for (int i = 0; i < m; i++) {
				for (int j = 0; j < n; j++) {
					if (j == n-1)
						System.out.println(distanceTable[i][j]);
					else
						System.out.print(distanceTable[i][j]+" ");
				}
			}
		}

	}

	public static class MyScanner {
		BufferedReader br;
		StringTokenizer st;

		public MyScanner() {
			br = new BufferedReader(new InputStreamReader(System.in));
		}

		String next() {
			while (st == null || !st.hasMoreElements()) {
				try {
					st = new StringTokenizer(br.readLine());
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			return st.nextToken();
		}

		int nextInt() {
			return Integer.parseInt(next());
		}
	}
}

class Point {
	final int x;
	final int y;

	Point(int x, int y) {
		this.x = x;
		this.y = y;
	}
}