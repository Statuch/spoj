package solved.PT07Y;

import java.io.IOException;
import java.util.LinkedList;

public class Main {

    static boolean[] IS_VISITED;
    static LinkedList<Integer>[] graph;

    public static void main(String[] args) throws IOException {
        int nodes = readInt();
        int edges = readInt();

        if (nodes != edges + 1) {
            print(false);
            return;
        }

        IS_VISITED = new boolean[nodes + 1];
        graph = new LinkedList[nodes+1];

        for (int i = 0; i < edges; i++) {
            int from = readInt();
            int to = readInt();
            if (from == to) {
                print(false);
                return;
            }
            if(graph[from] == null) graph[from] = new LinkedList<>();
            graph[from].add(to);
            if(graph[to] == null) graph[to] = new LinkedList<>();
            graph[to].add(from);
        }

        print(nodes == achivableNodes(graph, 1, 0));
    }

    static int achivableNodes(LinkedList<Integer>[] graph, int currentNode, int parent) {
        IS_VISITED[currentNode] = true;
        int visitedNodes = 1;
        for (int tmpNeighbour : graph[currentNode]) {
            if (tmpNeighbour == parent) continue;
            if (IS_VISITED[tmpNeighbour]) return -1;
            int subNodes = achivableNodes(graph, tmpNeighbour, currentNode);
            if (subNodes < 0) return subNodes;
            visitedNodes += subNodes;
        }
        return visitedNodes;
    }

    private static int readInt() throws IOException {
        int ret = 0;
        boolean dig = false;
        for (int c = 0; (c = System.in.read()) != -1; ) {
            if (c >= '0' && c <= '9') {
                dig = true;
                ret = ret * 10 + c - '0';
            } else if (dig) break;
        }
        return ret;
    }

    static void print(boolean isTree) {
        System.out.print(isTree ? "YES" : "NO");
    }
}