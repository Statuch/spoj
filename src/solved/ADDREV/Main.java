package solved.ADDREV;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class Main
{
	public static void main( String[] args ) {
		MyScanner scanner = new MyScanner();
		for( int tests = scanner.nextInt(); tests > 0; tests-- ) {
			System.out.println( revert( revert( scanner.nextLong() ) + revert( scanner.nextLong() ) ) );
		}
		scanner.close();
	}

	public static long revert( long val ) {
		long result = 0;
		while( val > 0 ) {
			result = (result * 10) + (val % 10);
			val = (val - (val % 10)) / 10;
		}
		return result;
	}

	public static class MyScanner
	{
		BufferedReader br;
		StringTokenizer st;

		public MyScanner() {
			br = new BufferedReader( new InputStreamReader( System.in ) );
		}

		String next() {
			while( st == null || !st.hasMoreElements() ) {
				try {
					st = new StringTokenizer( br.readLine() );
				} catch( IOException e ) {
					e.printStackTrace();
				}
			}
			return st.nextToken();
		}

		int nextInt() {
			return Integer.parseInt( next() );
		}

		long nextLong() {
			return Long.parseLong( next() );
		}

		void close() {
			try {
				br.close();
			} catch( IOException e ) {}
		}
	}
}
