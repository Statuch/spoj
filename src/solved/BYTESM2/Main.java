package solved.BYTESM2;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Scanner;
import java.util.StringTokenizer;

public class Main {
	public static PrintWriter out;

	public static void main(String[] args) {
		out = new PrintWriter(new BufferedOutputStream(System.out));
		MyScanner scanner = new MyScanner();

		for (int tests = scanner.nextInt(); tests > 0; tests--) {
			int height = scanner.nextInt();
			int width = scanner.nextInt();
			int prevStonesRow[] = new int[width];

			// initial prev row
			for(int j = 0; j<width; j++) {
				prevStonesRow[j] = scanner.nextInt();
			}
			
			for (int i = 1; i < height; i++) {
				int currentStonesRow[] = new int[width];

				for (int j = 0; j < width; j++) {
					int max = Integer.MIN_VALUE;

					if (j > 0) {
						max = prevStonesRow[j - 1];
					}
					if (prevStonesRow[j] > max) {
						max = prevStonesRow[j];
					}
					if (j < width - 1 && prevStonesRow[j + 1] > max) {
						max = prevStonesRow[j + 1];
					}
					currentStonesRow[j] = max + scanner.nextInt();
				}
				prevStonesRow = currentStonesRow;
			}
			int result = Integer.MIN_VALUE;
			for (int stonesAmount : prevStonesRow) {
				if (stonesAmount > result) {
					result = stonesAmount;
				}
			}
			System.out.println(result);
		}
		out.close();
	}
	
	public static class MyScanner {
		BufferedReader br;
		StringTokenizer st;

		public MyScanner() {
			br = new BufferedReader(new InputStreamReader(System.in));
		}

		String next() {
			while (st == null || !st.hasMoreElements()) {
				try {
					st = new StringTokenizer(br.readLine());
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			return st.nextToken();
		}

		int nextInt() {
			return Integer.parseInt(next());
		}

		long nextLong() {
			return Long.parseLong(next());
		}

		double nextDouble() {
			return Double.parseDouble(next());
		}

		String nextLine() {
			String str = "";
			try {
				str = br.readLine();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return str;
		}
	}
}
