package solved.BALLSUM;

import java.util.Scanner;

public class Main
{

	public static void main( String[] args ) {
		Main main = new Main();
		Scanner scanner = new Scanner( System.in );
		for( int N = scanner.nextInt(), K = scanner.nextInt(); N > 0; N = scanner.nextInt(), K = scanner.nextInt() ) {
			System.out.println( main.calculatePropability( N, K ) );
		}
	}

	public String calculatePropability( long N, long K ) {
		long combinations = N * (N - 1) / 2;
		long smallPairs = ((K-(K%2))/2) * ((K-1)/2) ;
		if( smallPairs == 0  ) {
			return "0";
		} else {
			long gcd = gcd( combinations, smallPairs );
			return (smallPairs / gcd) + "/" + (combinations / gcd);
		}
	}

	public long gcd( long a, long b ) {
		return b == 0 ? a : gcd( b, a % b );
	}
}
