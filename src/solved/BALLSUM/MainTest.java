package solved.BALLSUM;

import org.junit.Assert;
import org.junit.Test;

public class MainTest
{
	Main underTests = new Main();

	@Test
	public void standardTest(){
		Assert.assertEquals( "0", underTests.calculatePropability( 3, 2 ) );
		Assert.assertEquals( "2/2475", underTests.calculatePropability( 100, 5 ) );
		Assert.assertEquals( "2/15", underTests.calculatePropability( 10, 6 ) );
	}

	@Test
	public void largeNumbersTest(){
		Assert.assertEquals( "499999999/999999999", underTests.calculatePropability( 1000000000, 1000000000 ) );
		Assert.assertEquals( "0", underTests.calculatePropability( 1000000000, 0 ) );
		Assert.assertEquals( "0", underTests.calculatePropability( 0, 0 ) );
	}
}
