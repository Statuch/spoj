package solved.COINS;

import java.util.Scanner;

public class Main {
	static int maxArrayIndex = 10000000;
	static int[] coinsValues = new int[maxArrayIndex + 1];
	static int last = 0;

	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);
		while (scanner.hasNextLong()) {
			int val = scanner.nextInt();
			while (last <= val && last <= maxArrayIndex) {
				int tmpVal = coinsValues[last / 2];
				tmpVal += coinsValues[last / 3];
				tmpVal += coinsValues[last / 4];

				if (tmpVal < last) {
					coinsValues[last] = last;
				} else {
					coinsValues[last] = tmpVal;
				}
				last++;
			}
			System.out.println(get(val));

		}
		scanner.close();
	}

	static long get(int value) {
		if (value <= maxArrayIndex) {
			return coinsValues[value];
		} else {
			long tmpValue = get(value / 2) + get(value / 3) + get(value / 4);
			if (tmpValue > value)
				return tmpValue;
			else
				return value;
		}
	}
}
