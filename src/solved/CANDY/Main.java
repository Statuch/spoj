package solved.CANDY;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		for (int i = scanner.nextInt(); i >= 0; i = scanner.nextInt()) {
			int[] packs = new int[i];
			long sum = 0;
			for (int j = 0; j < i; j++) {
				packs[j] = scanner.nextInt();
				sum += packs[j];
			}
			System.out.println(solve(packs, sum));
		}
		scanner.close();
	}

	static long solve(int[] packs, long sum) {
		if (sum % packs.length != 0)
			return -1;
		else {
			long avg = sum / packs.length;
			int result = 0;
			for (int pack : packs) {
				if (pack > avg)
					result += pack - avg;
				else
					result += avg - pack;
			}
			return result/2;
		}
	}
}
