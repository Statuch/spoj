package solved.OFFSIDE;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class Main
{

	public static void main( String[] args ) {
		MyScanner scanner = new MyScanner();
		int attackersNo = scanner.nextInt();
		int deffendersNo = scanner.nextInt();
		while(attackersNo != 0 && deffendersNo != 0){
			int attackingPlayerPossition = Integer.MAX_VALUE;
			for(int i = 0; i<attackersNo; i++){
				int playerPossition = scanner.nextInt();
				if(playerPossition < attackingPlayerPossition) attackingPlayerPossition = playerPossition;
			}
			int goalKeeperPossition = Integer.MAX_VALUE;
			int lastDefenderPossition = Integer.MAX_VALUE;
			for(int i = 0; i<deffendersNo; i++){
				int deffendingPlayerPossition = scanner.nextInt();
				if(deffendingPlayerPossition < goalKeeperPossition ){
					lastDefenderPossition = goalKeeperPossition;
					goalKeeperPossition = deffendingPlayerPossition;
				}
				else if(deffendingPlayerPossition <lastDefenderPossition){
					lastDefenderPossition = deffendingPlayerPossition;
				}
			}
			
			System.out.println( attackingPlayerPossition < lastDefenderPossition ? 'Y' : 'N' );
			attackersNo = scanner.nextInt();
			deffendersNo = scanner.nextInt();
		}
		scanner.close();
	}

	public static class MyScanner
	{
		BufferedReader br;
		StringTokenizer st;

		public MyScanner() {
			br = new BufferedReader( new InputStreamReader( System.in ) );
		}

		String next() {
			while( st == null || !st.hasMoreElements() ) {
				try {
					st = new StringTokenizer( br.readLine() );
				} catch( IOException e ) {
					e.printStackTrace();
				}
			}
			return st.nextToken();
		}

		int nextInt() {
			return Integer.parseInt( next() );
		}

		void close() {
			try {
				br.close();
			} catch( IOException e ) {}
		}
	}

}
