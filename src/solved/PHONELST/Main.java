package PHONELST;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;


public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        for (int tests = scanner.nextInt(); tests > 0; tests--) {
            int phoneNumbers = scanner.nextInt();
            boolean prefixFound = false;
            Trie trie = new Trie();
            for (scanner.nextLine(); phoneNumbers > 0; phoneNumbers--) {
                String phoneNumber = scanner.nextLine();
                prefixFound = trie.add(phoneNumber) || prefixFound;
            }
            System.out.println(prefixFound ? "NO" : "YES");
        }
        scanner.close();
    }
}

class Node {
    boolean isEndOfPhoneNumber = false;
    Map<Character, Node> childNodes = new HashMap<>();

    boolean add(char[] phoneNumber, int position) {
        if (isEndOfPhoneNumber) {
            return true;
        }
        if (position != phoneNumber.length) {
            Node nextNode;
            if (!childNodes.containsKey(phoneNumber[position])) {
                nextNode = new Node();
                childNodes.put(phoneNumber[position], nextNode);
            } else {
                nextNode = childNodes.get(phoneNumber[position]);
            }
            return nextNode.add(phoneNumber, position + 1);
        } else {
            isEndOfPhoneNumber = true;
            return !childNodes.isEmpty();
        }
    }
}

class Trie extends Node {

    boolean add(String phoneNumber) {
        return add(phoneNumber.toCharArray(), 0);
    }
}
