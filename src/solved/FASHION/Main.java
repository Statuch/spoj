package solved.FASHION;

import java.util.Arrays;
import java.util.Scanner;

public class Main {
public static void main(String[] args) {
	Scanner scanner = new Scanner(System.in);
	for(int tests = scanner.nextInt(); tests>0; tests--) {
		int compatitors = scanner.nextInt();
		int[] men = new int[compatitors];
		
		for(int i = 0 ; i< compatitors; i++) {
			men[i] = scanner.nextInt();
		}
		Arrays.sort(men);
		
		int []women = new int[compatitors];
		for(int i = 0 ; i< compatitors; i++) {
			women [i] = scanner.nextInt();
		}
		Arrays.sort(women);
		
		long sum = 0;
		for(int i = 0; i<compatitors; i++) {
			sum += men[i]*women[i];
		}
		System.out.println(sum);
	}
}
}
