#include <stdio.h>

int min(int a, int b){
		return a>b ? b : a;
}

int min(int a, int b, int c){
	return min( min(a,b), c);
}
int min(int a, int b, int c, int d){
	return min( min(a,b), min(c,d));
}

int main(){	
	int rows;
	scanf("%d", &rows);

	long leftNode, middleNode, rightNode;
	int tmpLeft, tmpMiddle, tmpRight;
	
	for (int i = 1; rows > 0; i++, scanf("%d", &rows)) {

		scanf("%d %d %d", &leftNode, &middleNode, &rightNode);
		rightNode = middleNode+rightNode;
		leftNode=middleNode+1;
		for (int row = 1; row < rows; row++) {
			
			scanf("%d %d %d", &tmpLeft, &tmpMiddle, &tmpRight);
		
			tmpLeft += min(leftNode, middleNode);
			tmpMiddle += min(tmpLeft, leftNode, middleNode, rightNode);
			tmpRight += min(tmpMiddle, middleNode, rightNode);
			
			leftNode = tmpLeft;
			middleNode = tmpMiddle;
			rightNode = tmpRight;
		}
		printf("%d. %lu\n", i, middleNode);
	
	}
    return 0;
}
