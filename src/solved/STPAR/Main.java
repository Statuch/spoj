package solved.STPAR;

import java.util.Scanner;
import java.util.Stack;

public class Main {

	private static Stack<Integer> stack = new Stack<>();
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int trucks = scanner.nextInt();
		boolean result = true;
		while(trucks > 0) {
			int nextToGo = 1;
			for(int i = 0; i<trucks; i++) {
				while(!stack.isEmpty() && nextToGo ==stack.lastElement()) {
					nextToGo++;
					stack.pop();
				}
				int currentTruck = scanner.nextInt();
				if(currentTruck == nextToGo) {
					nextToGo++;
					continue;
				}
				else {
					if(!stack.isEmpty() && currentTruck > stack.lastElement()) {
						result = false;
					}
					else {
						stack.push(currentTruck);
					}
				}
			}
			System.out.println(result ? "yes" : "no");
			stack.clear();
			result = true;
			trucks = scanner.nextInt();
		}
		scanner.close();
	}	
}
