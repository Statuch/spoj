package solved.ACODE;

import java.io.*;
import java.util.StringTokenizer;

public class Main
{

	public static PrintWriter out;
	public static MyScanner scanner = new MyScanner();

	public static void main( String[] args ) {

		Main task = new Main();
		out = new PrintWriter( new BufferedOutputStream( System.out ) );

		for( String code = scanner.nextLine(); !code.startsWith( "0" ); code = scanner.nextLine() ) {
			out.println( task.solve( code.toCharArray() ) );
		}
		out.close();
	}

	public long solve( char[] numbers ) {
		long prev = 1;
		long oneBefore = 1;

		int last = numbers.length - 1;
		for( int i = last - 1; i >= 0; i-- ) {
			long tmp = prev;
			if( in( numbers[i] ) * in( numbers[i + 1] ) * (i + 2 > last ? 1 : in( numbers[i + 2] )) > 0
					&& (10 * in( numbers[i] )) + in( numbers[i + 1] ) <= 26 ) {
				prev +=  oneBefore;
				oneBefore = tmp;
			} else {
				oneBefore = prev;
			}
		}
		return prev;
	}

	int in( char number ) {
		return number - '0';
	}

	public static class MyScanner
	{
		BufferedReader br;
		StringTokenizer st;

		public MyScanner() {
			br = new BufferedReader( new InputStreamReader( System.in ) );
		}

		String next() {
			while( st == null || !st.hasMoreElements() ) {
				try {
					st = new StringTokenizer( br.readLine() );
				} catch( IOException e ) {
					e.printStackTrace();
				}
			}
			return st.nextToken();
		}

		String nextLine() {
			String str = "";
			try {
				str = br.readLine();
			} catch( IOException e ) {
				e.printStackTrace();
			}
			return str;
		}
	}
}
