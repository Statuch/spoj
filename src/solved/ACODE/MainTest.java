package solved.ACODE;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class MainTest {

	Main task = new Main();
	
	@Test
	public void testSolve() {
		//1
		assertEquals(1, task.solve("9".toCharArray()));
		assertEquals(1, task.solve("6".toCharArray()));
		assertEquals(1, task.solve("27".toCharArray()));
		assertEquals(1, task.solve("10".toCharArray()));
		assertEquals(1, task.solve("20".toCharArray()));
		assertEquals(1, task.solve("120".toCharArray()));
		assertEquals(1, task.solve("102".toCharArray()));
		assertEquals(1, task.solve("101010101010".toCharArray()));
		assertEquals(1, task.solve("220220220220".toCharArray()));
		assertEquals(1, task.solve("110220110520".toCharArray()));
		assertEquals(1, task.solve("2201".toCharArray()));
		assertEquals(1, task.solve("27201".toCharArray()));
		
		assertEquals(1, task.solve("6666666666666666666".toCharArray()));
		assertEquals(1, task.solve("3333333333333333333".toCharArray()));
		
		//2
		assertEquals(2, task.solve("19".toCharArray()));
		assertEquals(2, task.solve("16".toCharArray()));
		assertEquals(2, task.solve("26".toCharArray()));
		assertEquals(2, task.solve("22".toCharArray()));
		assertEquals(2, task.solve("1110".toCharArray()));
		assertEquals(2, task.solve("2220".toCharArray()));
		assertEquals(2, task.solve("2022".toCharArray()));
		assertEquals(2, task.solve("2666666666666666666".toCharArray()));
		
		//big results
		assertEquals(2, task.solve("11".toCharArray()));
		assertEquals(3, task.solve("111".toCharArray()));
		assertEquals(5, task.solve("1111".toCharArray()));
		assertEquals(8, task.solve("11111".toCharArray()));
		assertEquals(13, task.solve("111111".toCharArray()));
		assertEquals(21, task.solve("1111111".toCharArray()));
		assertEquals(34, task.solve("11111111".toCharArray()));
		assertEquals(55, task.solve("111111111".toCharArray()));
		assertEquals(89, task.solve("1111111111".toCharArray()));
		assertEquals(144, task.solve("11111111111".toCharArray()));
		assertEquals(233, task.solve("111111111111".toCharArray()));
		assertEquals(377, task.solve("1111111111111".toCharArray()));
		assertEquals(610, task.solve("11111111111111".toCharArray()));
		assertEquals(987, task.solve("111111111111111".toCharArray()));
		assertEquals(1597, task.solve("1111111111111111".toCharArray()));
	
		//resutl
		assertEquals(16, task.solve("25252525".toCharArray()));

		
	}

}
