package solved.PARKET1;

import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

public class MainTest
{
	Main underTest;
	private final static ByteArrayOutputStream OUT = new ByteArrayOutputStream();

	@BeforeClass public static void initSystem() {
		System.setOut( new PrintStream( OUT ) );
	}

	@Before public void init() {
		underTest = new Main();
		OUT.reset();
	}

	@Test public void taskTestCases() {
		underTest.solve( 16, 9 );
		Assert.assertEquals( "5 5", new String( OUT.toByteArray() ) );
	}

	@Test public void twoRowsSmall(){
		underTest.solve( 6, 0 );
		Assert.assertEquals( "3 2", new String( OUT.toByteArray() ) );
	}

	@Test public void twoRowsMedium(){
		underTest.solve( 1000, 0 );
		Assert.assertEquals( "500 2", new String( OUT.toByteArray() ) );
	}

	@Test public void twoRowsLarge(){
		underTest.solve( 100000, 0 );
		Assert.assertEquals( "50000 2", new String( OUT.toByteArray() ) );
	}

	//50.000 - szerokosc bedzie 12501
	@Test public void maxValues(){
		underTest.solve( 50000, 156225001 );
		Assert.assertEquals( "12501 12501", new String( OUT.toByteArray() ) );
	}
}
