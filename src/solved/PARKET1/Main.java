package solved.PARKET1;

import java.util.Scanner;

public class Main
{
	public static void main( String[] args ) {
		Scanner scanner = new Scanner( System.in );
		long A = scanner.nextInt();
		long B = scanner.nextInt();

		new Main().solve( A, B );
		scanner.close();

	}

	void solve( long outher, long inner ) {
		double sqrtDelta = Math.sqrt( ((outher + 4) * (outher + 4) / 4) - (4 * (outher + inner)) );
		long y = (long) (((outher / 2 + 2) + sqrtDelta) / 2);
		long x = (long) (((outher / 2 + 2) - sqrtDelta) / 2);
		System.out.print( x > y ? x + " " + y  : y + " " + x);
	}
}
