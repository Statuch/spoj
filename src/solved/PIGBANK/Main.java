package solved.PIGBANK;

import java.io.IOException;

public class Main {

    private final static int VALUE = 0;
    private final static int WEIGHT = 1;

    public static void main(String[] args) throws IOException {
        for (int tests = readInt(); tests > 0; tests--) {
            int emptyPigWeight = readInt();
            int pigWeight = readInt();

            int coinsNo = readInt();
            int[][] coins = new int[coinsNo][2];
            for (int i = 0; i < coinsNo; i++) {
                coins[i][VALUE] = readInt();
                coins[i][WEIGHT] = readInt();
            }
            int result = calculateMinMoneyValue(coins, pigWeight - emptyPigWeight);
            System.out.println(result >= 0 ? "The minimum amount of money in the piggy-bank is " + result + "." : "This is impossible.");
        }
    }

    private static int calculateMinMoneyValue(int[][] valueWeight, int totalWeight) {
        int[] minValuePerWeight = new int[totalWeight + 1];

        for (int i = 0, coins = valueWeight.length; i < coins; i++) {
            int coinWeight = valueWeight[i][WEIGHT];
            int coinValue = valueWeight[i][VALUE];
            for (int currWeight = 1; currWeight <= totalWeight; currWeight++) {

                int weightExclCurCoin = currWeight - coinWeight;
                if (weightExclCurCoin >= 0 && minValuePerWeight[weightExclCurCoin]>=0) {
                    int tmpValue = minValuePerWeight[weightExclCurCoin]+coinValue;
                    minValuePerWeight[currWeight] = minValuePerWeight[currWeight] <=0 || minValuePerWeight[currWeight] > tmpValue ? tmpValue : minValuePerWeight[currWeight];
                } else {
                    minValuePerWeight [currWeight] =  minValuePerWeight [currWeight] > 0 ? minValuePerWeight [currWeight] : -1;
                }
            }
        }
        return minValuePerWeight[totalWeight];
    }

    private static int readInt() throws IOException {
        int ret = 0;
        boolean dig = false;
        for (int c = 0; (c = System.in.read()) != -1; ) {
            if (c >= '0' && c <= '9') {
                dig = true;
                ret = ret * 10 + c - '0';
            } else if (dig) break;
        }
        return ret;
    }

}
