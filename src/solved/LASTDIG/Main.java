package solved.LASTDIG;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		for (int len = scanner.nextInt(); len > 0; len--) {
			int a = scanner.nextInt();
			long b = scanner.nextLong();
			System.out.println(solve(a, b));
		}
	}

	public static int solve(int a, long b) {
		List<Integer> lastDigits = new ArrayList<>();
		int val = a % 10;
		
		if(b==0) return 1;
		do {
			lastDigits.add(val);
			val = (val * a) % 10;
		} while (!lastDigits.contains(val));

		return lastDigits.get((int) ((b-1) % lastDigits.size()));
	}
}
