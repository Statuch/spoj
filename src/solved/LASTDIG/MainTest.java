package solved.LASTDIG;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

class MainTest {

	@Test
	void test() {
		assertEquals(9, Main.solve(3, 10l));
		assertEquals(6, Main.solve(6, 2l));
		assertEquals(1, Main.solve(6, 0l));
		assertEquals(0, Main.solve(0, 4l));

	}

}
