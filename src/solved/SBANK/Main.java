package solved.SBANK;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class Main {
		public static void main(String[] args) {
			Scanner scanner = new Scanner(System.in);
			for (int t = scanner.nextInt(); t > 0; t--) {
				List<String> array = new ArrayList<>();
				for (int accountNo = scanner.nextInt(); accountNo > 0; accountNo--) {
					array.add(scanner.nextLine());
				}
				scanner.nextLine();
				Collections.sort(array);
				String prev = "";
				int counter = 0;

				for (String account : array) {
					if (!prev.equals(account)) {
						System.out.println(" "+counter);
						System.out.print(account);
						counter = 1;

					} else {
						counter++;
					}
					prev = account;
				}
				// last counter
				if (counter > 1) {
					System.out.print(" " + counter);
				}
				System.out.println();
			}

	}
}
