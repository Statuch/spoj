package solved.ADAINDEX;

import org.junit.Assert;
import org.junit.Test;

public class MainTest
{
	@Test
	public void basicTest(){
		Main.Trie trie = new Main.Trie();
		trie.addWord( "dupa" );
		trie.addWord( "dup" );
		trie.addWord( "du" );
		trie.addWord( "d" );
		trie.addWord( "" );

		Assert.assertEquals( 4, trie.countWordsWithPrefix( "d" ));
		Assert.assertEquals( 3, trie.countWordsWithPrefix( "du" ));
		Assert.assertEquals( 2, trie.countWordsWithPrefix( "dup" ));
		Assert.assertEquals( 1, trie.countWordsWithPrefix( "dupa" ));
		Assert.assertEquals( 0, trie.countWordsWithPrefix( "" ));
	}
}
