package solved.ADAINDEX;

import java.util.Scanner;

public class Main
{
	public static void main( String[] args ) {
		Scanner scanner = new Scanner( System.in );
		int wordsInToDoList = scanner.nextInt();
		int queriesNumber = scanner.nextInt();
		scanner.nextLine();
		Trie trie = new Trie();
		for(int i = 0; i<wordsInToDoList; i++){
			trie.addWord( scanner.nextLine() );
		}
		for(int i = 0; i<queriesNumber; i++){
			System.out.println(trie.countWordsWithPrefix( scanner.nextLine() ));
		}
	}
	static class Trie extends Node{
		public void addWord(String word){
			Node tmpNode = this;
			for (char letter : word.toCharArray() ){
				Node child = tmpNode.childes[ getLetterIndex( letter ) ];
				if( child == null){
					child = new Node();
					tmpNode.childes[ getLetterIndex( letter ) ] = child;
				}
				child.occurs++;
				tmpNode = child;
			}
		}
		public int countWordsWithPrefix(String prefix){
			int result = 0;
			Node tmpNode = this;
			for(char letter : prefix.toCharArray()){
				tmpNode = tmpNode.childes[getLetterIndex( letter )];
				if(tmpNode == null) return 0;
				else {
					result = tmpNode.occurs;
				}
			}
			return result;
		}
		private int getLetterIndex (char letter){
			return letter-'a';
		}

	}


	static class Node{
		int occurs = 0;
		Node[] childes = new Node['z'-'a'+1];
	}
}
