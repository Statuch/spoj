package solved.EIGHTS;
import java.math.BigDecimal;
import java.util.Scanner;

public class Main {
	public static BigDecimal v192 = new BigDecimal("192");
	public static BigDecimal v250 = new BigDecimal("250");
	public static BigDecimal one = BigDecimal.ONE;

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		for (int tests = Integer.parseInt(scanner.nextLine()); tests > 0; tests--) {
			System.out.println(new BigDecimal(scanner.nextLine()).subtract(one).multiply(v250).add(v192).toString());
		}
		scanner.close();
	}
}
