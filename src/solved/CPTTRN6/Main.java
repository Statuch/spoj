package solved.CPTTRN6;

import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		for(int patterns = scanner.nextInt(); patterns>0; patterns--) {
			int horizontalBars = scanner.nextInt();
			int verticalBars = scanner.nextInt();
			int segmentHeight = scanner.nextInt();
			int segmentWidth = scanner.nextInt();

			int height = horizontalBars+ ((horizontalBars+1)*segmentHeight);
			int width = verticalBars+ ((verticalBars+1)*segmentWidth);
			
			for(int y = 0; y<height; y++) {
				for(int x = 0; x<width; x++) {
					char ch = '.';
					if(x%(segmentWidth+1)==segmentWidth ) {
						ch = '|';
					}
					if(y%(segmentHeight+1)==segmentHeight ) {
						if(ch =='|') {
							ch = '+';
						}
						else {
							ch= '-';
						}
						
					}
					System.out.print(ch);
				}
				System.out.println();
			}
			System.out.println();
		}
		scanner.close();
	}
}
