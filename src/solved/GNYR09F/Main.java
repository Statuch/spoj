package solved.GNYR09F;

import java.util.Scanner;

public class Main {
    private static int MAX_BITS = 100;
    private static int MAX_ADJACENT = 100;

    private static int NUMBER_OF_STATES = 2;
    private static int LOW = 0;
    private static int HIGH = 1;

    private int[][][] solutionMatrix = new int[MAX_BITS + 1][MAX_ADJACENT + 1][NUMBER_OF_STATES];

    private void initSolutionMatrix() {
        solutionMatrix[0][0][LOW] = 1;
        solutionMatrix[0][0][HIGH] = 1;

        for (int currentBitsLeft = 1; currentBitsLeft <= MAX_BITS; currentBitsLeft++) {
            solutionMatrix[currentBitsLeft][0][LOW] = solutionMatrix[currentBitsLeft - 1][0][LOW] + solutionMatrix[currentBitsLeft - 1][0][HIGH];
            solutionMatrix[currentBitsLeft][0][HIGH] = solutionMatrix[currentBitsLeft - 1][0][LOW];

        }
        for (int currentAdjRequired = 1; currentAdjRequired <= MAX_ADJACENT; currentAdjRequired++) {
            for (int currentBitsLeft = 1; currentBitsLeft <= MAX_BITS; currentBitsLeft++) {
                for (int currentState = LOW; currentState <= HIGH; currentState++) {
                    int currentSolutions = solutionMatrix[currentBitsLeft - 1][currentAdjRequired - currentState][HIGH];
                    currentSolutions += solutionMatrix[currentBitsLeft - 1][currentAdjRequired][LOW];
                    solutionMatrix[currentBitsLeft][currentAdjRequired][currentState] = currentSolutions;

                }
            }
        }
    }

    public static void main(String[] args) throws java.lang.Exception {
        Main main = new Main();
        main.initSolutionMatrix();

        Scanner scanner = new Scanner(System.in);
        for (int tests = scanner.nextInt(); tests > 0; tests--) {
            int testNumber = scanner.nextInt();
            int numberOfBits = scanner.nextInt();
            int requiredAdjacent = scanner.nextInt();
            System.out.println(testNumber + " " + main.solutionMatrix[numberOfBits][requiredAdjacent][LOW]);
        }
        scanner.close();
    }
}
