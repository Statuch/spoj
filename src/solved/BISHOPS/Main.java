package solved.BISHOPS;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Scanner;

public class Main {

	private static final BigInteger TWO = new BigInteger("2");
	public static PrintWriter out = new PrintWriter(new BufferedOutputStream(System.out), true);

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		while (sc.hasNextBigInteger()) {
			BigInteger result = sc.nextBigInteger();
			if (!result.equals(BigInteger.ONE)) {
				result = result.subtract(BigInteger.ONE).multiply(TWO);
			}
			out.println(result.toString());
		}
		out.flush();
		sc.close();
		out.close();
	}
}
