package solved.NY10A;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class MainTest
{
	Main underTest;

	@Before
	public void before(){
		underTest = new Main();
	}

	@Test
	public void whenHsOnlyThen38HHH(){
		int[] result = underTest.solve( "HHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH".toCharArray() );
		Assert.assertEquals(0, result[0]);
		Assert.assertEquals(38, result[7]);

	}
	@Test
	public void whenTsOnlyThen38TTT(){
		int[] result = underTest.solve( "TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT".toCharArray() );
		Assert.assertEquals(38, result[0]);
		Assert.assertEquals(0, result[7]);
	}

	@Test
	public void randomTest1(){
		int[] result = underTest.solve( "HHTTTHHTTTHTHHTHHTTHTTTHHHTHTTHTTHTTTHTH".toCharArray() );
		Assert.assertEquals(4, result[0]);
		Assert.assertEquals(7, result[1]);
		Assert.assertEquals(6, result[2]);
		Assert.assertEquals(4, result[3]);
		Assert.assertEquals(7, result[4]);
		Assert.assertEquals(4, result[5]);
		Assert.assertEquals(5, result[6]);
		Assert.assertEquals(1, result[7]);
	}

	@Test
	public void randomTest2(){
		int[] result = underTest.solve( "HTHTHHHTHHHTHTHHHHTTTHTTTTTHHTTTTHTHHHHT".toCharArray() );

		Assert.assertEquals(6, result[0]);
		Assert.assertEquals(3, result[1]);
		Assert.assertEquals(4, result[2]);
		Assert.assertEquals(5, result[3]);
		Assert.assertEquals(3, result[4]);
		Assert.assertEquals(6, result[5]);
		Assert.assertEquals(5, result[6]);
		Assert.assertEquals(6, result[7]);
	}
}
