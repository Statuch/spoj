package solved.NY10A;

import java.util.Scanner;

public class Main
{
	public static void main( String[] args ) {
		Scanner scanner = new Scanner( System.in );
		Main main = new Main();
		for( int P = scanner.nextInt(); P > 0; P-- ) {
			int caseNo = scanner.nextInt();
			int[] result = main.solve( scanner.next().toCharArray() );
			System.out.print(caseNo);
			for(int i = 0; i<8; i++){
				System.out.print( " " + result[i] );
			}
			System.out.println();
			scanner.nextLine();
		}
		scanner.close();
	}

	public int[] solve( char[] tosses ) {
		int[] result = new int[8];
		int index = 0;
		for( int i = 0; i < 40; i++ ) {
			index *= 2;
			if( tosses[i] == 'H' ) {
				index++;
			}
			index %= 8;
			if( i >= 2 ) {
				result[index]++;
			}
		}
		return result;
	}
}
