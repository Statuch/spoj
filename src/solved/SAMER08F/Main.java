package solved.SAMER08F;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		for (int N = scanner.nextInt(); N > 0; N = scanner.nextInt()) {
			long solution = 0;
			for (int i = 1; i <= N; i++) {
				solution += (N - i + 1) * (N - i + 1);
			}
			System.out.println(solution);
		}
		scanner.close();
	}
}
