package solved.BOMARBLE;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while(scanner.hasNext()){
            int pentagons = scanner.nextInt();
            if(pentagons>0) System.out.println(calculateMarbles(pentagons));
        }
        scanner.close();
    }

    private static int calculateMarbles(int N) {
        int result = 1 + 4*N;

        if(N>=2){
            result += N%2 != 0 ? ((N-1)/2)*3 * (N) : ((N)/2) * (N-1)*3;
        }
        return result;
    }
}
