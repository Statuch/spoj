package solved.CPTTRN2;

import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		for (int patterns = scanner.nextInt(); patterns > 0; patterns--) {
			int y = scanner.nextInt();
			int x = scanner.nextInt();
			for (int i = 0; i < y; i++) {

				for (int j = 0; j < x; j++) {
					if (i > 0 && j > 0 && i < y - 1 && j < x - 1) {
						System.out.print(".");
					} else {
						System.out.print("*");
					}
				}
				System.out.println();
			}
			System.out.println();
		}
		scanner.close();
	}
}
