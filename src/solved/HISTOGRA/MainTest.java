package solved.HISTOGRA;

import org.junit.Assert;
import org.junit.Test;

public class MainTest {
    @Test
    public void tests(){
        Assert.assertEquals(8, Main.findMaxRect(2, 1, 4, 5, 1, 3, 3 , 0));
    }

    @Test
    public void allEqualsTest(){
        Assert.assertEquals(4000, Main.findMaxRect(1000, 1000, 1000, 1000 , 0));
    }

    @Test
    public void singleValueTests(){
        Assert.assertEquals(1000, Main.findMaxRect(1000 , 0));
        Assert.assertEquals(0, Main.findMaxRect(0 , 0));
        Assert.assertEquals(1, Main.findMaxRect(1 , 0));
    }

    @Test
    public void goingUpTest(){
        Assert.assertEquals(36, Main.findMaxRect(0,1,2,3,4,5,6,7,8,9,10,11 , 0));
    }

    @Test
    public void goingDownTest(){
        Assert.assertEquals(36, Main.findMaxRect(11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1 , 0));
    }

    @Test
    public void upAndDownTest(){
        Assert.assertEquals(100, Main.findMaxRect(0,100,0,100,0,100,0,100, 0));
        Assert.assertEquals(400, Main.findMaxRect(0,100,0,200,0,300,0,400, 0));
    }

    @Test
    public void otherDevsTest(){
        Assert.assertEquals(3, Main.findMaxRect(2, 1, 2, 0));
        Assert.assertEquals(8, Main.findMaxRect(2,1,2,0,3,2,2,3,0));

    }
}