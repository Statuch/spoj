package solved.HISTOGRA;

import java.io.IOException;
import java.util.LinkedList;

public class Main {

    public static void main(String[] args) throws IOException {
        for (int bars = readInt(); bars > 0; bars = readInt()) {

            int[] barsArray = new int[bars+1];
            for (int i = 0; i < bars; i++) {
                barsArray[i] = readInt();
            }
            barsArray[bars] = 0;
            System.out.println(findMaxRect(barsArray));
        }
    }

    public static long findMaxRect(int... bars) {
        long result = 0;
        LinkedList<Bar> barsList = new LinkedList<>();
        for (int index = 0, len = bars.length; index < len; index++) {
            var currentHigh = bars[index];

            Bar removed = null;
            while (!barsList.isEmpty() && barsList.peekFirst().height > currentHigh) {
                removed = barsList.pollFirst();
                var candidate = removed.height * (index - removed.position);
                result = candidate > result ? candidate : result;

            }
            if(removed != null){
                removed.height = bars[index];
                barsList.push(removed);
            }
            if (barsList.isEmpty() || bars[index] > barsList.peekFirst().height) {
                barsList.push(new Bar(index, bars[index]));
            }
        }
        return result;
    }

    static class Bar {
        long position, height;

        Bar(long position, long height) {
            this.position = position;
            this.height = height;
        }
    }

    private static int readInt() throws IOException {
        int ret = 0;
        boolean dig = false;
        for (int c = 0; (c = System.in.read()) != -1; ) {
            if (c >= '0' && c <= '9') {
                dig = true;
                ret = ret * 10 + c - '0';
            } else if (dig) break;
        }
        return ret;
    }
}

