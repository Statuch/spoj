package solved.HANGOVER;

import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
		Main task = new Main();
		Scanner scanner = new Scanner(System.in);
		for (double val = scanner.nextDouble(); val > 0; val = scanner.nextDouble()) {
			System.out.println(task.solve(val) + " card(s)");
		}
		scanner.close();
	}

	private int solve(double max) {

		double counter = 2.;
		for (double val = 0.5; val <= max; val += 1. / counter) {
			counter++;
		}
		return (int) counter - 1;
	}
}
