package solved.EC_CONB;

import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {
        for (int tests = readInt(); tests > 0; tests--) {
            int value = readInt();
            int result = value;
            if (result % 2 == 0) {
                for (result = 0; value > 0; value = value >> 1) {
                    result = (result << 1) | (value & 1);
                }
            }
            System.out.println(result);
        }
    }

    private static int readInt() throws IOException {
        int ret = 0;
        boolean dig = false;
        for (int c = 0; (c = System.in.read()) != -1; ) {
            if (c >= '0' && c <= '9') {
                dig = true;
                ret = ret * 10 + c - '0';
            } else if (dig) break;
        }
        return ret;
    }
}
