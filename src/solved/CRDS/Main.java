package solved.CRDS;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		for (int tests = scanner.nextInt(); tests > 0; tests--) {
			long T = scanner.nextLong();
			long result = ((3*T*T)+T)/2;
			System.out.println(result%1000007);
		}
		scanner.close();
	}
}
