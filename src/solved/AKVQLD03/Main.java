package solved.AKVQLD03;

import java.io.*;
import java.util.StringTokenizer;

public class Main
{
	int[] funsDistribution;
	int officesNo = 0;

	Main( int offices ) {
		int arraySize = 1;
		while( arraySize < offices ) {
			arraySize *= 2;
		}

		officesNo = offices;
		funsDistribution = new int[arraySize * 2];
	}

	void add( int office, int funs ) {
		int fromRange = 1;
		int toRange = officesNo;
		int midRange = getRangeMiddle( fromRange, toRange );
		for( int i = 1; i < funsDistribution.length; ) {
			funsDistribution[i] += funs;
			i *= 2;
			if( office <= midRange ) {
				toRange = midRange;
			} else {
				i++;
				fromRange = midRange + 1;
			}
			midRange = getRangeMiddle( fromRange, toRange );
		}
	}

	int getRangeMiddle( int from, int to ) {
		return (from + to ) / 2;
	}

	int query( int officeA, int officeB ) {
		return queryInRange( 1, 1, officesNo, officeA, officeB );
	}

	int queryInRange( int index, int fromRange, int toRange, int officeA, int officeB ) {

		int midRange = getRangeMiddle( fromRange, toRange );
		if( fromRange >= officeA && toRange <= officeB ) {
			return funsDistribution[index];
		} else if( officeA <= toRange && officeB >= fromRange ) {
			return queryInRange( index * 2, fromRange, midRange, officeA, officeB ) + queryInRange( index * 2 + 1, midRange + 1,
					toRange, officeA, officeB );
		} else {
			return 0;
		}
	}

	public static void main( String[] args ) {
		MyScanner scanner = new MyScanner( );
		out = new PrintWriter(new BufferedOutputStream(System.out), true);

		Main main = new Main( scanner.nextInt() );
		int queriesNo = scanner.nextInt();
		for( ; queriesNo > 0; queriesNo-- ) {

			String line[] = scanner.nextLine().split( " " );
			int a = Integer.parseInt( line[1] );
			int b = Integer.parseInt( line[2] );
			switch( line[0] ) {
				case "add":
					main.add( a, b );
					break;
				default:
					out.println( main.query( a, b ) );
					break;
			}
		}
		out.close();
	}
	// -----------PrintWriter for faster output---------------------------------
	public static PrintWriter out;

	// -----------MyScanner class for faster input----------
	public static class MyScanner {
		BufferedReader br;
		StringTokenizer st;

		public MyScanner() {
			br = new BufferedReader(new InputStreamReader(System.in));
		}

		String next() {
			while (st == null || !st.hasMoreElements()) {
				try {
					st = new StringTokenizer(br.readLine());
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			return st.nextToken();
		}

		int nextInt() {
			return Integer.parseInt(next());
		}

		long nextLong() {
			return Long.parseLong(next());
		}

		double nextDouble() {
			return Double.parseDouble(next());
		}

		String nextLine() {
			String str = "";
			try {
				str = br.readLine();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return str;
		}
	}
	// --------------------------------------------------------
}

