package solved.BUGLIFE;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

public class Main {

    public static void main(String[] args) throws IOException {
        for (int testNo = 1, tests = readInt(); testNo <= tests; testNo++) {
            System.out.println("Scenario #" + testNo + ":");

            Bug[] bugsSexualRecord = new Bug[readInt() + 1];
            for (int actions = readInt(); actions > 0; actions--) {
                int firstBugIdx = readInt();
                Bug firsBug = bugsSexualRecord[firstBugIdx];
                if (firsBug == null) {
                    firsBug = new Bug(firstBugIdx);
                    bugsSexualRecord[firstBugIdx] = firsBug;
                }
                int secondBugIdx = readInt();
                Bug secondBug = bugsSexualRecord[secondBugIdx];
                if (secondBug == null) {
                    secondBug = new Bug(secondBugIdx);
                    bugsSexualRecord[secondBugIdx] = secondBug;
                }

                firsBug.partners.add(secondBug);
                secondBug.partners.add(firsBug);
            }
            System.out.println(conductExperiment(bugsSexualRecord) ? "No suspicious bugs found!" : "Suspicious bugs found!" );
        }
    }

    static boolean conductExperiment(Bug[] bugSexualLife) {
        for (Bug bug : bugSexualLife) {
            if (bug != null && (bug.gender == Gender.UNSPECIFIED && !startExperimentWith(bug))) {
                return false;
            }
        }
        return true;
    }

    static private boolean startExperimentWith(Bug bug) {
        bug.gender = Gender.A;
        for (Bug partner : bug.partners) {
            if (experiment(bug, partner) == false) return false;
        }
        return true;
    }

    static private boolean experiment(Bug previousPartner, Bug bug) {
        bug.gender = previousPartner.gender.getOpositGender();
        for (Bug partner : bug.partners) {
            if (partner.gender == Gender.UNSPECIFIED && !experiment(bug, partner)) {
                return false;
            } else if (previousPartner.equals(partner)) {
                continue;
            } else if (partner.gender == bug.gender) {
                return false;
            }
        }
        return true;
    }

    private static int readInt() throws IOException {
        int ret = 0;
        boolean dig = false;
        for (int c = 0; (c = System.in.read()) != -1; ) {
            if (c >= '0' && c <= '9') {
                dig = true;
                ret = ret * 10 + c - '0';
            } else if (dig) break;
        }
        return ret;
    }

    static class Bug {
        final int id;

        Bug(int number) {
            id = number;
        }

        Gender gender = Gender.UNSPECIFIED;
        List<Bug> partners = new LinkedList<>();

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Bug bug = (Bug) o;
            return id == bug.id;
        }

        @Override
        public int hashCode() {
            return id;
        }
    }

    enum Gender {
        UNSPECIFIED, A, B;

        Gender getOpositGender(){
            return (this == B || this == UNSPECIFIED) ? A : B;
        }
    }
}
