package solved.ANARC09A;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main
{
	private static char[] equation;
	public static void main( String[] args ) {

		MyScanner scanner = new MyScanner( );

		int tests = 1;
		for(equation = scanner.nextLine(); equation[0] != '-'; equation = scanner.nextLine()){
			int bugs = 0;
			int tmpState = 0;
			for(char ch : equation){
				if(ch == '{'){
					tmpState++;
				}
				else if(tmpState > 0){
					tmpState--;
				}
				else{
					bugs++;
				}
			}
			int result = bugs%2 + tmpState%2;
			result += (bugs + tmpState - result )/2;
			System.out.println(tests + ". "+ result);
			tests++;
		}
	}


	public static class MyScanner {
		BufferedReader br;

		public MyScanner() {
			br = new BufferedReader(new InputStreamReader(System.in));
		}

		char[] nextLine() {
			try {
				return br.readLine().toCharArray();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return null;
		}
	}
}
