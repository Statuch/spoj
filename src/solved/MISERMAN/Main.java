package solved.MISERMAN;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int N = scanner.nextInt();
        int M = scanner.nextInt();
        int costs[] = new int[M];
        for (int i = 0; i < N; i++) {
            int tmpCost[] = new int[M];
            for (int j = 0; j < M; j++) {
                tmpCost[j] = min(costs, j - 1, j + 1) + scanner.nextInt();
            }
            costs = tmpCost;
        }
        System.out.println(min(costs, 0, M-1));
    }

    static int min(int[] array, int from, int to) {
        from = from < 0 ? 0 : from;
        to = to > array.length - 1 ? array.length - 1 : to;
        int min = Integer.MAX_VALUE;
        while (from <= to) {
            min = min < array[from] ? min : array[from];
            from++;
        }
        return min;
    }
}
