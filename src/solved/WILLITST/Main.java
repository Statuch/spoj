package solved.WILLITST;

import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		long n = scanner.nextLong();
		String result = "TAK";
		long one = 1;
		while (n > 1) {
			if ((n & one) == one && n>1) {
				result = "NIE";
				break;
			} else {
				n = n >> 1;
			}
		}
		System.out.println(result);
		scanner.close();
	}
}
