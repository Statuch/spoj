package solved.CPTTRN1;

import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		for(int patterns = scanner.nextInt(); patterns >0; patterns--) {
			int x = scanner.nextInt();
			int y = scanner.nextInt();
			char[] oddRow = new char[y];
			char prevChar = '.';
			char [] evenRow = new char[y];
			for(int i = 0; i<y; i++) {
				prevChar = prevChar == '.' ? '*' : '.';
				evenRow[i] = prevChar ;
			}
			prevChar = '*';
			for(int i = 0; i<y; i++) {
				prevChar = prevChar == '.' ? '*' : '.';
				oddRow[i] = prevChar ;
			}
			
			for(int i = 0; i<x; i++) {
				if(i%2 == 0) {
					System.out.println(evenRow);
				}else {
					System.out.println(oddRow);
				}
			}
			System.out.println();
			
		}
		scanner.close();
	}
}
