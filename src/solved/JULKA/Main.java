package solved.JULKA;

import java.math.BigInteger;
import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		for (int i = 0; i < 10; i++) {
			BigInteger a = new BigInteger(scanner.nextLine());
			BigInteger b = new BigInteger(scanner.nextLine());			
			
			BigInteger natalia = a.subtract(b).divide(new BigInteger("2"));
			BigInteger julia = natalia.add(b);
			System.out.println(julia);
			System.out.println(natalia);
		}
		scanner.close();
	}
}
/*
 * String aSubB = sub(a, b); String natalia = half(aSubB); String julka =
 * add(natalia, b); System.out.println(julka); System.out.println(natalia); }
 * scanner.close(); }
 * 
 * public static String add(String as, String bs) { StringBuffer result = new
 * StringBuffer(); int credit =0; for(int la = as.length()-1, lb =bs.length()-1;
 * la>=0 || lb >=0; la--, lb--) { int a = 0; int b = 0; if(la>=0) { a =
 * as.charAt(la)-'0'; } if(lb>=0) { b = bs.charAt(lb)-'0'; } int tmpResult =
 * a+b+credit; if(tmpResult >9) { credit = (tmpResult - (tmpResult%10))/10; }
 * else { credit = 0; } result.append(tmpResult%10); } if(credit>0) {
 * result.append(credit); } result = result.reverse(); int i= 0; for(int len =
 * result.length(); i<len;i++) { if(result.charAt(i) != '0') { break; } } String
 * r =result.substring(i).toString(); if(r.length()<1) return "0"; else return
 * r; }
 * 
 * public static String sub(String a, String b) { StringBuffer result = new
 * StringBuffer(); int debt = 0; for (int al = a.length()-1, bl = b.length()-1;
 * al >= 0; al--, bl--) { char alast = a.charAt(al); char blast = '0'; if(bl
 * >=0) { blast =b.charAt(bl); } int tmpResult = alast - blast - debt; if
 * (tmpResult < 0) { debt = 1; tmpResult +=10; } else { debt= 0; }
 * result.append(tmpResult); } return result.reverse().toString(); }
 * 
 * public static String half(String a) { StringBuffer result = new
 * StringBuffer(); int tmpVal =0; for(int i = 0, len = a.length(); i<len;i++ ) {
 * int digit = a.charAt(i)-'0'; tmpVal =(tmpVal * 10) + digit ; if(tmpVal >= 2)
 * { result.append((tmpVal - (tmpVal%2))/2 ); tmpVal = 0; } else
 * if(result.length()>0) { result.append("0"); } } if(result.length()<1) {
 * return "0"; } return result.toString(); } }
 */