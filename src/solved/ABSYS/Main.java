package solved.ABSYS;

import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		for (int test = Integer.parseInt(scanner.nextLine()); test > 0; test--) {
			scanner.nextLine();
			String equation = scanner.nextLine();
			int plusInd = equation.indexOf("+");
			int eqInd = equation.indexOf("=");

			String first = equation.substring(0, plusInd - 1);
			String second = equation.substring(plusInd + 2, eqInd - 1);
			String result = equation.substring(eqInd + 2);

			if (first.contains("machula")) {
				first = "" + (Integer.parseInt(result) - Integer.parseInt(second));
			} else if (second.contains("machula")) {
				second = "" + (Integer.parseInt(result) - Integer.parseInt(first));
			} else {
				result = "" + (Integer.parseInt(first) + Integer.parseInt(second));
			}
			System.out.println(first + " + " + second + " = " + result);
		}
		scanner.close();
	}
}
