package solved.STAMPS;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.StringTokenizer;

public class Main
{

	public static void main( String[] args ) {
		MyScanner scanner = new MyScanner( );
		PrintWriter out = new PrintWriter( new BufferedOutputStream( System.out ) );

		int scenario = 1;
		for( int tests = scanner.nextInt(); tests > 0; tests-- ) {
			int missings = scanner.nextInt();
			int friends = scanner.nextInt();
			List<Integer> friendsStamps = new ArrayList<>();
			for( int i = 0; i < friends; i++ ) {
				friendsStamps.add( scanner.nextInt() );
			}
			Collections.sort( friendsStamps, new Comparator<Integer>() {@Override
			public int compare( Integer o1, Integer o2 ) {
				return o2 - o1;
			}} );
			int frinedNo = 0;
			while( frinedNo < friends && missings > 0 ) {
				missings -= friendsStamps.get( frinedNo );
				frinedNo++;
			}
			out.println( "Scenario #" + scenario +":");
			if( missings > 0 ) {
				out.println( "impossible" );
			} else {
				out.println( frinedNo );
			}
			out.println();
			scenario++;
		}
		out.flush();
		out.close();
	}
	
			public static class MyScanner {
			BufferedReader br;
			StringTokenizer st;

			public MyScanner() {
				br = new BufferedReader(new InputStreamReader(System.in));
			}

			String next() {
				while (st == null || !st.hasMoreElements()) {
					try {
						st = new StringTokenizer(br.readLine());
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
				return st.nextToken();
			}

			int nextInt() {
				return Integer.parseInt(next());
			}

			long nextLong() {
				return Long.parseLong(next());
			}

			double nextDouble() {
				return Double.parseDouble(next());
			}

			String nextLine() {
				String str = "";
				try {
					str = br.readLine();
				} catch (IOException e) {
					e.printStackTrace();
				}
				return str;
			}
		}

}
