
package solved.QCJ1;

import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		char[] path = new char[200];
		int length = 0;
		int N = Integer.parseInt(scanner.nextLine());
		scanner.nextLine();
		for (; N > 0; N--) {

			char[] line = scanner.nextLine().toCharArray();

			length = length > line.length ? length : line.length;
			for (int i = 0; i < length; i++) {
				if (line[i] != ' ') {
					path[i] = line[i];
				}
			}
		}

		System.out.println("Total Walk Distance = " + length);
		String direction = getDirection(path[0]);
		int distance = 1;
		for (int i = 1; i < length; i++) {
			if (path[i - 1] == path[i]) {
				distance++;
			}
			if (path[i - 1] != path[i] ) {
				System.out.println(direction + " " + distance + " steps");
				distance = 1;
				direction = getDirection(path[i]);
			}
			if(i +1 == length) {
				System.out.println(direction + " " + distance + " steps");
			}
		}
		
		scanner.close();
	}

	private static String getDirection(char step) {
		switch (step) {
		case '_':
			return "Walk";
		case '/':
			return "Up";
		default:
			return "Down";
		}
	}
}
