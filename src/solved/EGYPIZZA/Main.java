package solved.EGYPIZZA;

import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
		int quoters = 0;
		int halfs = 0;
		int threeForths = 0;
		int result = 1;

		Scanner scanner = new Scanner(System.in);
		for (int friends = Integer.parseInt(scanner.nextLine()); friends > 0; friends--) {
			String sliceSize = scanner.nextLine();
			if ("1/4".equals(sliceSize)) {
				quoters++;
			} else if ("1/2".equals(sliceSize)) {
				halfs++;
			} else {
				threeForths++;
			}
		}

		int min = quoters > threeForths ? threeForths : quoters;
		quoters -= min;
		result += threeForths;

		result += halfs / 2;
		quoters += 2 * (halfs % 2);

		result += quoters / 4;
		if (quoters % 4 > 0)
			result++;

		System.out.println(result);
		scanner.close();
	}
}
