package solved.BLINNET;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        for (int tests = scanner.nextInt(); tests > 0; tests--) {
            List<Road> roads = new ArrayList<>();
            DisjointSet<Integer> disjointSets = new DisjointSet<>();
            int numberOfCities = scanner.nextInt();
            for (int i = 1; i <= numberOfCities; i++) {
                disjointSets.addSet(i);
            }
            for (int cityNumber = 1; cityNumber <= numberOfCities; cityNumber++) {
                scanner.nextLine();
                scanner.nextLine();
                for (int i = 1, numberOfNeighbours = scanner.nextInt(); i <= numberOfNeighbours; i++) {
                    roads.add(new Road(cityNumber, scanner.nextInt(), scanner.nextInt()));
                }
            }

            Collections.sort(roads);
            long totalLength = 0;
            for (Road road : roads) {
                if (disjointSets.joinSets(road.cityA, road.cityB)) {
                    totalLength += road.length;
                }
            }
            System.out.println(totalLength);
        }
        scanner.close();
    }
}

class Road implements Comparable<Road> {
    final int cityA, cityB, length;

    Road(int cityA, int cityB, int length) {
        this.cityA = cityA;
        this.cityB = cityB;
        this.length = length;
    }

    @Override
    public int compareTo(Road o) {
        return length - o.length;
    }
}

class DisjointSet<T> {
    private Map<T, SubSet<T>> subsets = new HashMap<>();

    void addSet(T value) {
        subsets.put(value, new SubSet<>(value));
    }

    boolean joinSets(T subSetA, T subSetB) {
        T subSetARepresentative = findSet(subSetA);
        T subSetBRepresentative = findSet(subSetB);
        if (subSetARepresentative == null || subSetBRepresentative == null || subSetARepresentative.equals(subSetBRepresentative)) {
            return false;
        } else {
            subsets.get(subSetARepresentative).parent = subsets.get(subSetBRepresentative);
            return true;
        }
    }

    private T findSet(T subSetValue) {
        SubSet<T> selectedSubset = subsets.get(subSetValue);
        while (selectedSubset != null && selectedSubset != selectedSubset.parent) {
            selectedSubset = selectedSubset.parent;
        }
        return selectedSubset != null ? selectedSubset.node : null;
    }

    class SubSet<E> {
        E node;
        SubSet<E> parent;

        private SubSet(E node) {
            this.node = node;
            this.parent = this;
        }

        @Override
        public int hashCode() {
            return node.hashCode();
        }
    }
}
