package solved.NGM;

import java.util.Scanner;

public class Main
{
	public static void main( String[] args ) {
		Scanner scanner = new Scanner(System.in);
		long number = scanner.nextLong();
		long firstStep = number%10;
		if(firstStep > 0){
			System.out.println( "1" );
			System.out.print(firstStep);
		}
		else{
			System.out.print("2");
		}
		scanner.close();
	}
}
