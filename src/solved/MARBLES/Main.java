package solved.MARBLES;

import java.util.Scanner;

public class Main {


    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        for(int tests = scanner.nextInt(); tests>0; tests--){
            int items = scanner.nextInt();
            int colors = scanner.nextInt();

            if(items==colors){
                System.out.println("1");
            }
            else{
                System.out.println(solve(colors, items-colors));
            }
        }
    }

    static long solve(int colors, int marblesToPick){
        long[] values = new long[colors+1];
        for(int i = 1; i<=colors; i++){
            values[i]=i;
        }

        for(int i = 1; i<marblesToPick; i++){
            for(int j = 2; j<=colors;j++){
                values[j]+=values[j-1];
            }
        }
        return values[colors];
    }
}
