package solved.JAVAC;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		while (scanner.hasNext()) {
			String varName = scanner.nextLine();
			System.out.println(convertVarName(varName.toCharArray()));
		}
		scanner.close();
	}

	public static String convertVarName(char[] name) {
		StringBuilder newVarName = new StringBuilder();

		boolean isJavaName = true;
		boolean isCppName = true;

		for (int i = 0, len = name.length; i < len; i++) {
			char currentChar = name[i];

			if (Character.isUpperCase(currentChar)) {
				if (!isJavaName || i == 0) {
					return "Error!";
				}
				isCppName = false;
				newVarName.append('_').append(Character.toLowerCase(currentChar));
			} else if (currentChar == '_') {
				if (!isCppName || i == 0 || i == len - 1) {
					return "Error!";
				}
				isJavaName = false;
				char fallowing = name[++i];
				if (Character.isLowerCase(fallowing)) {
					newVarName.append(Character.toUpperCase(fallowing));
				} else
					return "Error!";

			} else {
				newVarName.append(currentChar);
			}
		}
		return newVarName.toString();
	}
}
