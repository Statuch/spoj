package solved.CPRMT;

import java.util.Arrays;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Main cp = new Main();

        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNext()) {
            System.out.println(cp.getCommonPermuation(scanner.nextLine(), scanner.nextLine()));
        }
        scanner.close();
    }

    String getCommonPermuation(String a, String b) {
        StringBuffer buffer = new StringBuffer();
        char[] aArr = a.toCharArray();
        char[] bArr = b.toCharArray();

        Arrays.sort(aArr);
        Arrays.sort(bArr);

        int aStart = 0, aLastIdx = aArr.length;
        int bStart = 0, bLastIdx = bArr.length;

        while (aStart < aLastIdx && bStart < bLastIdx) {
            if (aArr[aStart] == bArr[bStart]) {
                buffer.append(aArr[aStart]);
                aStart++;
                bStart++;
            } else if (aArr[aStart] < bArr[bStart]) {
                aStart++;
            } else {
                bStart++;
            }
        }
        return buffer.toString();
    }
}
