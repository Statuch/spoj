package solved.ONP;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;
import java.util.Stack;

public class Main {
	private static Set<Character> opType = new HashSet<>();
	static {
		opType.add('+');
		opType.add('-');
		opType.add('/');
		opType.add('*');
		opType.add('^');
	}

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		for (int test = scanner.nextInt(); test >= 0; test--) {
			System.out.println(solve(scanner.nextLine()));
		}
	}

	public static String solve(String string) {
		StringBuffer result = new StringBuffer();
		Stack<Character> operations = new Stack<>();
		for(int i =0, len = string.length(); i<len;i++) {
			char curChr = string.charAt(i);
			if(opType.contains(curChr)) {
				operations.push(curChr);
			}
			else if(curChr == ')') {
				result.append(operations.pop());
			}
			else if(curChr == '(') {
				continue;
			}
			else {
				result.append(curChr);
			}
		}
		return result.toString();
	}
}
