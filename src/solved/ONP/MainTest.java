package solved.ONP;


import static org.junit.Assert.assertEquals;

import org.junit.Test;

class MainTest {

	@Test
	void test() {
		assertEquals("abc*+",Main.solve("(a+(b*c))") );
		assertEquals("ab+zx+*", Main.solve("((a+b)*(z+x))"));
		assertEquals("at+bac++cd+^*", Main.solve("((a+t)*((b+(a+c))^(c+d)))"));
		
		assertEquals("at+bac++cd+^*", Main.solve("((a+t)*((b+(a+c))^(c+d)))"));
	}

}
