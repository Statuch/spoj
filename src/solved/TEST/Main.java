package solved.TEST;

import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		while (scan.hasNext()) {
			String input = scan.nextLine();
			if ("42".equals(input))
				break;
			System.out.println(input);
		}
	}
}
