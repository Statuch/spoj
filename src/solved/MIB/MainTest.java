package solved.MIB;

import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class MainTest {

    @Test
    public void simpleTest(){
        Assert.assertEquals((4*120)+(0*24)+(2*6)+(1*2)+1+1, Main.getPermutationOrderNumber("5", "1", "4", "3", "6", "2").longValue());
    }

    @Test
    public void singleWordTest(){
        Assert.assertEquals(1, Main.getPermutationOrderNumber("1").longValue());
        Assert.assertEquals(1, Main.getPermutationOrderNumber("aaaaa").longValue());

    }
    @Test
    public void lastPermutationTest(){
        Assert.assertEquals(6*5*4*3*2, Main.getPermutationOrderNumber("6", "5", "4", "3", "2", "1").longValue());
        //Assert.assertEquals(5*4*3*2, Main.getPermutationOrderNumber("5", "4", "3", "2", "1").longValue());
        //Assert.assertEquals(4*3*2, Main.getPermutationOrderNumber("4", "3", "2", "1").longValue());
        //Assert.assertEquals(3*2, Main.getPermutationOrderNumber("3", "2", "1").longValue());
        //Assert.assertEquals(2, Main.getPermutationOrderNumber("2", "1").longValue());
        //Assert.assertEquals(1, Main.getPermutationOrderNumber( "1").longValue());
    }

    @Test
    public void firstPermutationTest(){
        Assert.assertEquals("1", Main.getPermutationOrderNumber("1", "11", "111", "1111", "11111","111111","1111111","11111111","111111111", "1111111111").toString());
        List<String> words = new ArrayList<>();
        for(int i = 1; i< 1000; i++){
            int digit = i / 100;
            var word = "digit";
            for(int count = i % 100; count >= 0; count--){
                word += digit;
                words.add(word);
            }
        }
        Assert.assertEquals("1", Main.getPermutationOrderNumber(words.toArray(new String[1000])).toString());
    }

    private static final class FactorialUtils{
        private static List<BigDecimal> FACTORIALS = new ArrayList<>();
        static {


            FACTORIALS.add(BigDecimal.ZERO);
            FACTORIALS.add(BigDecimal.ONE);
        }
        public BigDecimal getFactorial(int number){
            while(FACTORIALS.size()> number){
                int nextFactorialIndex = FACTORIALS.size();
                FACTORIALS.add(BigDecimal.valueOf(nextFactorialIndex).multiply(FACTORIALS.get(nextFactorialIndex-1)) );
            }
            return FACTORIALS.get(number);
        }
    }

}