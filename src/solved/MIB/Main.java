package solved.MIB;

import java.math.BigDecimal;
import java.util.*;

public class Main {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int testCases = Integer.parseInt(scanner.nextLine());
        for (int testNo = 0; testNo < testCases; testNo++) {
            int numbers = Integer.parseInt(scanner.nextLine());
            System.out.println(getPermutationOrderNumber(scanner.nextLine().split(" ")));
        }
    }

    public static BigDecimal getPermutationOrderNumber(String... wordsArray) {
        BigDecimal result = BigDecimal.ONE;

        LinkedList<String> knownWords = new LinkedList<>();
        knownWords.add(wordsArray[wordsArray.length - 1]);
        for (int wordIndex = wordsArray.length - 2; wordIndex >= 0; wordIndex--) {
            String currentWord = wordsArray[wordIndex];
            int orderedIndex = -Collections.binarySearch(knownWords, currentWord) - 1;
            result = result.add(new BigDecimal(orderedIndex).multiply(FactorialUtils.getFactorial(knownWords.size())));
            knownWords.add(orderedIndex, currentWord);
        }
        return result;
    }
    private static final class FactorialUtils {
        private static final List<BigDecimal> FACTORIALS = new ArrayList<>();

        static {
            FACTORIALS.add(BigDecimal.ZERO);
            FACTORIALS.add(BigDecimal.ONE);
        }
        public static BigDecimal getFactorial(int number) {
            while (FACTORIALS.size() <= number) {
                int nextFactorialIndex = FACTORIALS.size();
                FACTORIALS.add(BigDecimal.valueOf(nextFactorialIndex).multiply(FACTORIALS.get(nextFactorialIndex - 1)));
            }
            return FACTORIALS.get(number);
        }
    }
}
