package solved.PALIN;

import java.io.*;

public class Main {

	static BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
	static PrintWriter writer = new PrintWriter(new BufferedOutputStream(System.out), true);

	public static void main( String[] args ) throws IOException {
		Main main = new Main();
		int tests = Integer.parseInt( reader.readLine() );
		while(tests>0){
			main.printNextPalin( reader.readLine().toCharArray() );
			tests--;
		}
		writer.flush();
	}

	public void printNextPalin(char[] digits) throws IOException {
		int last = digits.length-1;
		increase(digits, last);
		if(digits[0]<='9'){
			for(int a = 0, b = digits.length-1; a<=b; a++, b--){
				if(digits[a] < digits[b]){
					increase( digits, b-1 );
				}
				digits[b] = digits[a];
			}
		}
		else{
			digits[0] = '0';
			writer.append( '1');
			digits[last]++;
		}
		writer.write( digits );
		writer.write( "\n\r" );
	}
	void increase(char[] digits, int start){
		digits[start]++;
		while(start>0 && digits[start] > '9'){
			digits[start] = '0';
			digits[--start]++;
		}
	}
}

