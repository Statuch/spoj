package solved.PALIN;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import java.io.*;

public class MainTest
{
	Main underTests = new Main();
	ByteArrayOutputStream output;
	@Before
	public void before(){
		output = new ByteArrayOutputStream(  );
		System.setOut( new PrintStream( output ) );
	}

	@Test
	public void test() throws IOException {
		underTests.printNextPalin( "9".toCharArray() );
		Assert.assertEquals( "11",  output.toString() );
	}

//	@Test
//	public void milionNinesTest() throws IOException {
//		System.setIn( this.getClass().getClassLoader().getResourceAsStream( "solved.PALIN/milionNines" ) );
//		Scanner resultScanner = new Scanner( this.getClass().getClassLoader().getResourceAsStream( "solved.PALIN/milionNinesPalin" ) );
//		Assert.assertEquals( resultScanner.nextLine(),  output.toString() );
//	}


}
