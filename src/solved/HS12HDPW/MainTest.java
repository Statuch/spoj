package solved.HS12HDPW;

import org.junit.Assert;
import org.junit.Test;

public class MainTest
{
	@Test
	public void testA(){
		Main main = new Main();
		char[] password = main.decode( new String[]{"qwe345","rf3Arg"}, "XSBSRasdew9873465hkldsfsalndfvnfq489uqovkLKJHaeDaae555Sk5asdpASD".toCharArray() );
		Assert.assertEquals("keep", new String(password) );
	}

	@Test
	public void testB(){
		Main main = new Main();
		char[] password = main.decode( new String[]{"2S4J5K", "111111", "lrtb2A"}, "isimgsow45ipfgisd56wfgngdfcdkgc7kKKKkuuJJgfstdygQdWORQADFSLKF2K8".toCharArray() );
		Assert.assertEquals("coding", new String(password) );
	}
}