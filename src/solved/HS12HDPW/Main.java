package solved.HS12HDPW;

import java.util.Scanner;

public class Main
{
	public static void main( String[] args ) {
		Main main = new Main();
		Scanner scanner = new Scanner( System.in );
		for( int tests = scanner.nextInt(); tests > 0; tests-- ) {
			int tuplesNo = scanner.nextInt();
			scanner.nextLine();

			String[] tuples = scanner.nextLine().split( " " );
			String key = scanner.nextLine();
			System.out.println(main.decode( tuples, key.toCharArray() ));
		}
		scanner.close();
	}

	char[] decode( String[] tuples, char[] map ) {
		char[] password = new char[tuples.length * 2];
		for( int i = 0, len = tuples.length; i < len; i++ ) {
			password[2*i] = map[ getFirstCharacterIdx(tuples[i]) ];
			password[2*i+1] = map[ getSecondCharacterIdx(tuples[i])];
		}
		return password;
	}

	int getFirstCharacterIdx(String tuple){
		char[] tupleChars = tuple.toCharArray();
		int mask = 1;
		int result = 0;
		for(int i = 0; i<6; i++){
			result += tupleChars[i] & mask;
			mask*=2;
		}
		return result;
	}

	int getSecondCharacterIdx(String tuple){
		char[] tupleChars = tuple.toCharArray();
		int result = 0;
		for(int i = 0; i<6; i++){
			if( (tupleChars[i] & (int)Math.pow(2, (i+3)%6 )) > 0 ){
				 result+=(int)Math.pow(2, i);
			}
		}
		return result;
	}
}
