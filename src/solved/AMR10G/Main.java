package solved.AMR10G;

import java.io.*;
import java.util.Arrays;
import java.util.StringTokenizer;

public class Main
{
	public static PrintWriter out = new PrintWriter(new BufferedOutputStream(System.out), true);

	public static void main( String[] args ) {
		MyScanner scanner = new MyScanner( );
		for( int tests = scanner.nextInt(); tests > 0; tests-- ) {
			int[] kidsHeight = new int[scanner.nextInt()];
			int souldiersNo = scanner.nextInt();
			for( int i = 0, len = kidsHeight.length; i < len; i++ ) {
				kidsHeight[i] = scanner.nextInt();
			}
			if(souldiersNo == 1){
				out.println("0");
				continue;
			}

			Arrays.sort( kidsHeight );

			int result = Integer.MAX_VALUE;
			for( int start = 0, end = souldiersNo - 1; end < kidsHeight.length; start++, end++ ) {
				int heightGap = kidsHeight[end] - kidsHeight[start];
				result = (result > heightGap) ? heightGap : result;
			}
			out.println( result );
		}
	}

	public static class MyScanner {
		BufferedReader br;
		StringTokenizer st;

		public MyScanner() {
			br = new BufferedReader(new InputStreamReader(System.in));
		}

		String next() {
			while (st == null || !st.hasMoreElements()) {
				try {
					st = new StringTokenizer(br.readLine());
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			return st.nextToken();
		}

		int nextInt() {
			return Integer.parseInt(next());
		}

		long nextLong() {
			return Long.parseLong(next());
		}

		double nextDouble() {
			return Double.parseDouble(next());
		}

		String nextLine() {
			String str = "";
			try {
				str = br.readLine();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return str;
		}
	}
}
