package solved.PARTY;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main
{
	public static void main( String[] args ) {
		Scanner scanner = new Scanner( System.in );
		for( int budget = scanner.nextInt(); budget >0 ; budget = budget = scanner.nextInt() ) {

			PartyPlanner planner = new PartyPlanner( budget );

			for(int partiesNo = scanner.nextInt(); partiesNo>0; partiesNo--){
				planner.parties.add( new Party( scanner.nextInt(), scanner.nextInt() ) );
			}
			PartyPlan plan = planner.planPartiesSchedule();
			System.out.println(plan.totalCost + " "+plan.totalFun);
		}
	}


	static class Party implements Comparable<Party>
	{
		int cost;
		int fun;

		Party( int cost, int fun ) {
			this.cost = cost;
			this.fun = fun;
		}

		@Override public int compareTo( Party o ) {
			if( o.cost != cost ) {
				return cost - o.cost;
			} else {
				return o.fun - fun ;
			}

		}
	}

	static class PartyPlanner{
		final int funds;
		List<Party> parties;

		PartyPlanner(int funds){
			this.funds = funds;
			parties = new ArrayList<>(  );
		}

		PartyPlan planPartiesSchedule(){
			parties.sort( Party::compareTo );

			int partiesNo = parties.size();
			int[][] solutionTable = new int[partiesNo][funds+1];

			int bestPartiesTotalFun = 0;
			int bestPartiesTotalCosts = Integer.MAX_VALUE;

			for( int i = 0; i < partiesNo; i++ ) {
				//for( int j = parties.get( i ).cost; j <= funds; j++ ) {
				for( int j = 1; j <= funds; j++ ) {
					if( j < parties.get( i ).cost ){
						solutionTable[i][j] = i>0?	solutionTable[i-1][j] : 0;
						continue;
					}
					int skipPartyFun = i>0 ? solutionTable[i-1][j] : 0;
					int goForPartyFun = parties.get( i ).fun;
					if (i > 0) goForPartyFun += solutionTable[i-1][j-parties.get( i ).cost];

					solutionTable[i][j] = goForPartyFun;
					if( skipPartyFun > goForPartyFun ){
						solutionTable[i][j] = skipPartyFun;
					}

					if(bestPartiesTotalFun < solutionTable[i][j]){
						bestPartiesTotalFun = solutionTable[i][j];
						bestPartiesTotalCosts = j;
					}
					else if(bestPartiesTotalFun == solutionTable[i][j]){
						bestPartiesTotalCosts = bestPartiesTotalCosts > j ? j : bestPartiesTotalCosts ;
					}

				}
			}

			PartyPlan plan = new PartyPlan();
			plan.totalCost = bestPartiesTotalCosts;
			plan.totalFun= bestPartiesTotalFun;
			return plan;
		}
	}

	static class PartyPlan{
		int totalCost;
		int totalFun;
	}
}
