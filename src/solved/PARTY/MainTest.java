package solved.PARTY;

import org.junit.Assert;
import org.junit.Test;

public class MainTest
{

	@Test
	public void problemExmapleA(){
		Main.PartyPlanner underTests = new Main.PartyPlanner(50);
		underTests.parties.add( new Main.Party( 12, 3 ) );
		underTests.parties.add( new Main.Party( 15, 8 ) );
		underTests.parties.add( new Main.Party( 16, 9) );
		underTests.parties.add( new Main.Party( 16, 6) );
		underTests.parties.add( new Main.Party( 10, 2) );
		underTests.parties.add( new Main.Party( 21, 9) );
		underTests.parties.add( new Main.Party( 18, 4) );
		underTests.parties.add( new Main.Party( 12, 4) );
		underTests.parties.add( new Main.Party( 17, 8) );
		underTests.parties.add( new Main.Party( 18, 9) );
		Main.PartyPlan result = underTests.planPartiesSchedule();

		Assert.assertEquals(26, result.totalFun);
		Assert.assertEquals(49, result.totalCost);
	}

	@Test
	public void problemExmapleB(){
		Main.PartyPlanner underTests = new Main.PartyPlanner(50);
		underTests.parties.add( new Main.Party( 13, 8 ) );
		underTests.parties.add( new Main.Party( 19, 10 ) );
		underTests.parties.add( new Main.Party( 16, 8 ) );
		underTests.parties.add( new Main.Party( 12, 9 ) );
		underTests.parties.add( new Main.Party( 10, 2 ) );
		underTests.parties.add( new Main.Party( 12, 8 ) );
		underTests.parties.add( new Main.Party( 13, 5 ) );
		underTests.parties.add( new Main.Party( 15, 5 ) );
		underTests.parties.add( new Main.Party( 11, 7) );
		underTests.parties.add( new Main.Party( 16, 2) );

		Main.PartyPlan result = underTests.planPartiesSchedule();

		Assert.assertEquals(32, result.totalFun);
		Assert.assertEquals(48 , result.totalCost);
	}
}
