package solved.ACPC10A;

import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		while (true) {
			int a = scanner.nextInt();
			int b = scanner.nextInt();
			int c = scanner.nextInt();

			if (a == 0 && b == 0 && c == 0) {
				break;
			} else {
				if (b - a == c - b) {
					System.out.println("AP " + (c - a + b));
				} else {
					System.out.println("GP " + ((c / a) * b));
				}
			}
		}
		scanner.close();

	}

}
