#include<stdio.h>

int main(){
	int tests =0;
	scanf("%d", &tests);
	while(tests){
		int arraySize, updates;
		scanf("%d %d", &arraySize, &updates);
		int  array [arraySize+1]; 
		
		for(int i =0; i<=arraySize; i++){
			array[i] = 0;
		}
		
		while(updates){
			int from, to, value;
			scanf("%d %d %d", &from, &to, &value);
			array[from]+=value;
			array[to+1]-=value;
			updates--;
		}

		// update table
		for(int i =1; i<=arraySize; i++){
			array[i]+=array[i-1];
		} 
		
		int queries, index=0;
		scanf("%d", &queries);
		while(queries){
			scanf("%d", &index);
			printf("%d\n", array[index]);
			queries--;
		}
		tests--;	
	}
	return 0;
}