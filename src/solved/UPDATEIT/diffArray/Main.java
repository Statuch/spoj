package solved.UPDATEIT.diffArray;

import java.io.*;

public class Main {
    static final PrintWriter out = new PrintWriter(new BufferedOutputStream(System.out), true);
    static int [] array = new int[10001];
    static boolean isFirstTest = true;
    public static void main(String[] args) throws IOException {
        for(int tests = readInt(); tests>0; tests--){

            int arraySize = readInt();
            cleanArray(array, arraySize);
            for(int updates = readInt(); updates>0; updates--){
                int from = readInt();
                int to = readInt();
                int value = readInt();
                array[from]+=value;
                array[to+1]-=value;
            }
            int diff = 0;
            for(int i = 0; i<arraySize; i++){
                diff+=array[i];
                array[i]=diff;
            }

            for(int queries = readInt(); queries>0; queries--){
                out.println(array[readInt()]);
            }
        }
    }

    private static int readInt() throws IOException {
        int ret = 0;
        boolean dig = false;
        for (int c = 0; (c = System.in.read()) != -1; ) {
            if (c >= '0' && c <= '9') {
                dig = true;
                ret = ret * 10 + c - '0';
            } else if (dig) break;
        }
        return ret;
    }

    private static void cleanArray(int[] array, int size){
        if(isFirstTest){
            isFirstTest=false;
            return;
        }

        for(int i = 0; i<size; i++){
            array[i] = 0;
        }
    }

}
