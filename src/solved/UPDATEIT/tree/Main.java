package solved.UPDATEIT.tree;

import java.io.*;
import java.util.StringTokenizer;

public class Main {

    static PrintWriter out = new PrintWriter(new BufferedOutputStream(System.out), true);
    static MyScanner scanner = new MyScanner();

    public static void main(String[] args) {
        for (int tests = scanner.nextInt(); tests > 0; tests--) {
            UpdateTree tree = new UpdateTree(scanner.nextInt());

            for (int updates = scanner.nextInt(); updates > 0; updates--) {
                tree.add(scanner.nextInt(), scanner.nextInt(), scanner.nextInt());
            }

            for (int queries = scanner.nextInt(); queries > 0; queries--) {
                out.println(tree.calculateSum(scanner.nextInt()));
            }
        }
    }

    public static class MyScanner {
        BufferedReader br;
        StringTokenizer st;

        public MyScanner() {
            br = new BufferedReader(new InputStreamReader(System.in));
        }

        String next() {
            while (st == null || !st.hasMoreElements()) {
                try {
                    st = new StringTokenizer(br.readLine());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return st.nextToken();
        }

        int nextInt() {
            return Integer.parseInt(next());
        }
    }
}

class TreeNode {
    private final int rangeStart, rangeEnd;
    private int updateValue;

    TreeNode(int start, int end) {
        rangeStart = start;
        rangeEnd = end;
    }

    private TreeNode leftChild, rightChild;

    void add(int from, int to, int value) {
        if (from <= rangeStart && to >= rangeEnd) {
            updateValue += value;
            return;
        }
        int midRange = (rangeStart + rangeEnd) / 2;

        if (doesRangesIntersect(rangeStart, midRange, from, to)) {
            leftChild = leftChild == null ? new TreeNode(rangeStart, midRange) : leftChild;
            leftChild.add(from, to, value);
        }

        if (doesRangesIntersect(midRange + 1, rangeEnd, from, to)) {
            rightChild = rightChild == null ? new TreeNode(midRange + 1, rangeEnd) : rightChild;
            rightChild.add(from, to, value);
        }
    }

    long calculateSum(int element) {
        if (element < rangeStart || element > rangeEnd) {
            return 0;
        }
        int sum = updateValue;
        if (leftChild != null) sum += leftChild.calculateSum(element);
        if (rightChild != null) sum += rightChild.calculateSum(element);

        return sum;
    }


    private boolean doesRangesIntersect(int startA, int endA, int startB, int endB) {
        return !(startA > endB || endA < startB);
    }
}

class UpdateTree extends TreeNode {
    UpdateTree(int elements) {
        super(0, elements - 1);
    }
}
