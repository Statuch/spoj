package solved.ACPC11B;

import java.io.IOException;
import java.util.Arrays;

public class Main {
    public static void main(String[] args) throws IOException {
        for(int tests = readInt(); tests>0;tests--){
            int[] firsMountain = readStationAltitude();
            Arrays.sort(firsMountain);
            int[] secondMountain = readStationAltitude();
            Arrays.sort(secondMountain);
            int score = Integer.MAX_VALUE;
            for(int i = firsMountain.length-1, j=secondMountain.length-1; i>=0 && j>=0;){
                if(firsMountain[i] == secondMountain[j]){
                    score = 0;
                    break;
                }
                else{
                    int tmpScore = firsMountain[i]-secondMountain[j];
                    if(tmpScore > 0){
                        i--;
                    }else{
                        j--;
                        tmpScore = -tmpScore;
                    }
                    score = score<tmpScore ? score : tmpScore;
                }
            }
            System.out.println(score);
        }

    }

    private static int[] readStationAltitude() throws IOException {
        int points = readInt();
        int[] stationAltitude = new int[points];
        for(int i = 0; i<points; i++){
            stationAltitude[i] = readInt();
        }
        return stationAltitude;
    }

    private static int readInt() throws IOException {
        int ret = 0;
        boolean dig = false;
        for (int c = 0; (c = System.in.read()) != -1; ) {
            if (c >= '0' && c <= '9') {
                dig = true;
                ret = ret * 10 + c - '0';
            } else if (dig) break;
        }
        return ret;
    }

}
