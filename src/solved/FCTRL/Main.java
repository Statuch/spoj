package solved.FCTRL;

import java.util.*;
import java.lang.*;

class Main {
	public static void main(String[] args) throws java.lang.Exception {
		Scanner scanner = new Scanner(System.in);
		for (int i = scanner.nextInt(); i > 0; i--) {
			int val = scanner.nextInt();
			int result = 0;

			for (int max = val - (val % 5), a = 5; a <= max; a *= 5) {
				result += (val - (val % a)) / a;
			}

			System.out.println(result);
		}
	}

	public static long Z(int I) {

		int result = 0;
		int max = I - (I % 5);

		for (int a = 5; a < max; a *= 5) {
			result += (I - (I % a)) / a;
		}

		return result;
	}
}
