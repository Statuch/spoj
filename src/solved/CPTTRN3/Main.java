package solved.CPTTRN3;

import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		for (int patterns = scanner.nextInt(); patterns > 0; patterns--) {
			int y = scanner.nextInt();
			int x = scanner.nextInt();
			int size = scanner.nextInt();
			y = (y * (size + 1)) + 1;
			x = (x * (size + 1)) + 1;
			for (int i = 0; i < y; i++) {

				for (int j = 0; j < x; j++) {
					if (i > 0 && j > 0 && i < y - 1 && j < x - 1 && (i % (size + 1) != 0) && (j % (size + 1) != 0)) {
						int offsetX = i % (size + 1);
						int offsetY = j % (size + 1);
						int col = (j - offsetY) / (size + 1);
						int row = (i - offsetX) / (size + 1);

						if (col % 2 == row % 2 && offsetX == offsetY) {
							System.out.print('\\');
						} else if (col % 2 != row % 2 && size-offsetX == offsetY-1) {
							System.out.print("/");
						}
						else {
							System.out.print(".");
						}

					} else {
						System.out.print("*");
					}
				}
				System.out.println();
			}
			System.out.println();
		}
		scanner.close();
	}
}
