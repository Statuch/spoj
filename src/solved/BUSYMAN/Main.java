package solved.BUSYMAN;

import java.io.*;
import java.util.Arrays;

public class Main {
    public static PrintWriter out = new PrintWriter(new BufferedOutputStream(System.out), true);

    public static void main(String[] args) throws IOException {
        for (int tests = readInt(); tests > 0; tests--) {
            int activitiesNo = readInt();
            Activity[] activities = new Activity[activitiesNo];
            for (int i = 0; i < activitiesNo; i++) {
                activities[i] = new Activity(readInt(), readInt());
            }
            out.println(mostActivities(activities));
        }
    }

    private static int mostActivities(Activity[] activities) {
        Arrays.sort(activities);
        int result = 0;
        int tmpEnd = 0;
        for (Activity activy : activities) {
            if (tmpEnd <= activy.start) {
                result++;
                tmpEnd = activy.end;
            }
        }
        return result;
    }


    private static int readInt() throws IOException {
        int ret = 0;
        boolean dig = false;
        for (int c = 0; (c = System.in.read()) != -1; ) {
            if (c >= '0' && c <= '9') {
                dig = true;
                ret = ret * 10 + c - '0';
            } else if (dig) break;
        }
        return ret;
    }
}

class Activity implements Comparable<Activity> {
    final int start;
    final int end;

    Activity(int start, int end) {
        this.start = start;
        this.end = end;
    }

    @Override
    public int compareTo(Activity o) {
        return end - o.end;
    }
}

