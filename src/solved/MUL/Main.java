package solved.MUL;

import java.io.IOException;
import java.math.BigDecimal;

public class Main {

    public static void main(String[] args) throws IOException {
        for (int tests = readInt(); tests > 0; tests--) {
            BigDecimal a = readNumberAsBigDecimal();
            BigDecimal b = readNumberAsBigDecimal();
            System.out.println(a.multiply(b).toString());
        }
    }

    private static int readInt() throws IOException {
        int ret = 0;
        boolean dig = false;
        for (int c = 0; (c = System.in.read()) != -1; ) {
            if (c >= '0' && c <= '9') {
                dig = true;
                ret = ret * 10 + c - '0';
            } else if (dig) break;
        }
        return ret;
    }

    private static BigDecimal readNumberAsBigDecimal() throws IOException {
        StringBuilder buffer = new StringBuilder();
        boolean dig = false;
        for (int c = 0; (c = System.in.read()) != -1; ) {
            if (c >= '0' && c <= '9') {
                dig = true;
                buffer.append((char) c);
            } else if (dig) break;
        }
        return new BigDecimal(buffer.toString());
    }

}