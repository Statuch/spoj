package solved.DCEPC206;

import org.junit.Assert;
import org.junit.Test;

import java.util.Random;

public class MainTest {

    @Test
    public void sampleTests() {
        Assert.assertEquals(15, Main.calculate(
                1, 5, 3, 6, 4
        ));
    }

    @Test
    public void singleValue(){
        Assert.assertEquals(0, Main.calculate(0));
        Assert.assertEquals(0, Main.calculate(1));
        Assert.assertEquals(0, Main.calculate(100));
    }
    @Test
    public void randomBrutForceTest() {

        Random random = new Random();

        for (int i = 0; i < 1000; i++) {
            var maxValue = 20;
            var testData = new int[]{
                    random.nextInt(maxValue), random.nextInt(maxValue), random.nextInt(maxValue),
                    random.nextInt(maxValue), random.nextInt(maxValue), random.nextInt(maxValue),
                    random.nextInt(maxValue), random.nextInt(maxValue), random.nextInt(maxValue),
                    random.nextInt(maxValue), random.nextInt(maxValue), random.nextInt(maxValue),
                    random.nextInt(maxValue), random.nextInt(maxValue), random.nextInt(maxValue),
                    random.nextInt(maxValue), random.nextInt(maxValue), random.nextInt(maxValue),
                    random.nextInt(maxValue), random.nextInt(maxValue), random.nextInt(maxValue),
                    random.nextInt(maxValue), random.nextInt(maxValue), random.nextInt(maxValue)};
            Assert.assertEquals("invalid result for: "+ arrayToString(testData),  brutforce(testData), Main.calculate(
                    testData
            ));

        }

    }

    private String arrayToString(int[] testData) {
        StringBuilder builder = new StringBuilder();
        for(int val: testData){
            if(builder.length() > 0){
                builder.append(",");
            }
            builder.append(val);
        }
        return "["+ builder.toString() + "]";
    }

    private int brutforce(int[] testData) {
        int total = 0;
        for(int i = 0, len = testData.length; i<len; i++){
            for(int j = i-1; j>=0; j--){
                total += testData[j] < testData[i] ? testData[j] : 0;
            }
        }
        return total;
    }
}