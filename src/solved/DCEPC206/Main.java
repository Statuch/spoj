package solved.DCEPC206;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.StringTokenizer;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {
        MyScanner scanner = new MyScanner();
        for (int test = scanner.nextInt(); test > 0; test--) {
            int T = scanner.nextInt();
            Step[] input = new Step[T];
            for (int i = 0; i < T; i++) {
                input[i] = new Step(scanner.nextInt());
            }
            var sorted = sortByMerging(input, 0, T - 1);
            var result = Arrays.stream(sorted).map(step -> step.value).reduce(0L, (a, b) -> a + b);
            System.out.println(result);
        }
    }

    public static long calculate(int... steps) {
        var arr = Arrays.stream(steps).mapToObj(Step::new).collect(Collectors.toList());
        var stepArr = arr.toArray(new Step[0]);
        var sorted = sortByMerging(stepArr, 0, stepArr.length - 1);
        return Arrays.stream(sorted).map(step -> step.value).reduce(0L, (result, step) -> result + step);
    }

    public static Step[] sortByMerging(Step[] input) {
        return sortByMerging(input, 0, input.length - 1);
    }

    public static Step[] sortByMerging(Step[] input, int start, int end) {

        if (end - start <= 1) {
            if (end == start) {
                return new Step[]{input[start]};
            }

            var result = new Step[end - start + 1];
            if (input[end].number > input[start].number) {
                result[0] = input[start];
                result[1] = input[end].increaseValue(input[start].number);
            } else {
                result[0] = input[end];
                result[1] = input[start];
            }


            return result;
        }
        return mergeAndCalculateValue(
                sortByMerging(input, start, (start + end + 1) / 2),
                sortByMerging(input, (start + end + 1) / 2 +1, end)
        );
    }

    private static Step[] mergeAndCalculateValue(Step[] left, Step[] right) {
        var result = new Step[right.length + left.length];
        long bonus = 0;

        var leftIterator = 0;
        var rightIterator = 0;

        for (int index = 0; leftIterator < left.length || rightIterator < right.length; index++) {

            if (leftIterator >= left.length) {
                result[index] = right[rightIterator++].increaseValue(bonus);
            } else if (rightIterator >= right.length) {
                result[index] = left[leftIterator++];
                //bonus += result[index].number;
            } else if (left[leftIterator].number < right[rightIterator].number) {
                result[index] = left[leftIterator++];
                bonus += result[index].number;
            } else {
                result[index] = right[rightIterator++].increaseValue(bonus);
            }
        }

        return result;
    }

    public static class MyScanner {
        BufferedReader br;
        StringTokenizer st;

        public MyScanner() {
            br = new BufferedReader(new InputStreamReader(System.in));
        }

        String next() {
            while (st == null || !st.hasMoreElements()) {
                try {
                    st = new StringTokenizer(br.readLine());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return st.nextToken();
        }

        int nextInt() {
            return Integer.parseInt(next());
        }
    }

    public static class Step {
        int number;
        long value;

        Step(int number) {
            this.number = number;
            this.value = 0;
        }

        Step increaseValue(long value) {
            this.value += value;
            return this;
        }
    }
}
