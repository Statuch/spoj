package solved.FARIDA;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;
import java.util.StringTokenizer;

public class Main {
	public static void main(String[] args) {
		MyScanner scanner = new MyScanner();

		for (long testCase = 1, tests = scanner.nextLong(); testCase <= tests; testCase++) {
			int trolls = scanner.nextInt();
			long[] trollsMoney = new long[trolls];
			for (int i = 0, len = trollsMoney.length; i < len; i++) {
				trollsMoney[i] = scanner.nextLong();
			}

			for (int i = 2, len = trolls; i < len; i++) {
				if (i == 2) {
					trollsMoney[i] += trollsMoney[ i - 2 ];
				} else {
					long tmp = trollsMoney[i] + trollsMoney[ i - 2 ];
					long tmp2 = trollsMoney[i] + trollsMoney[i - 3];
					trollsMoney[i] = tmp > tmp2 ? tmp : tmp2;
				}
			}
			long result = 0;
			
			if(trolls > 0){
				result = trollsMoney[trolls - 1];
			}
			
			if (trolls > 1) {
				result = result > trollsMoney[trolls - 2] ? result : trollsMoney[trolls - 2];
			}
			
			System.out.println("Case " + testCase + ": " + result);

		}
	}
	
	public static class MyScanner {
		BufferedReader br;
		StringTokenizer st;

		public MyScanner() {
			br = new BufferedReader(new InputStreamReader(System.in));
		}

		String next() {
			while (st == null || !st.hasMoreElements()) {
				try {
					st = new StringTokenizer(br.readLine());
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			return st.nextToken();
		}

		int nextInt() {
			return Integer.parseInt(next());
		}

		long nextLong() {
			return Long.parseLong(next());
		}
	}
}
