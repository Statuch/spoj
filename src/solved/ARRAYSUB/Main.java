package solved.ARRAYSUB;

import java.io.IOException;
import java.util.LinkedList;
import java.util.Scanner;

public class Main {
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        int arraySize = scanner.nextInt();
        int[] array = new int[arraySize];
        for (int i = 0; i < arraySize; i++) {
            array[i] = scanner.nextInt();
        }
        int windowSize = scanner.nextInt();

        LinkedList<Item> window = new LinkedList<>();
        for (int i = 0; i < arraySize; i++) {

            Item tmpItem = new Item(i, array[i]);
            while (!window.isEmpty() && window.getFirst().value <= tmpItem.value) {
                window.removeFirst();
            }

            window.addFirst(tmpItem);

            if (window.getLast().index + windowSize <= i) {
                window.removeLast();
            }

            if (i>= windowSize-1) {

                System.out.print(window.getLast().value + " ");

            }
        }
    }

    static class Item {
        final int index, value;

        Item(int index, int value) {
            this.index = index;
            this.value = value;
        }
    }

    private static int readInt() throws IOException {
        int ret = 0;
        boolean dig = false;
        for (int c = 0; (c = System.in.read()) != -1; ) {
            if (c >= '0' && c <= '9') {
                dig = true;
                ret = ret * 10 + c - '0';
            } else if (dig) break;
        }
        return ret;
    }
}
