package solved.BEENUMS;

import java.util.Scanner;

public class Main
{
	static Scanner scanner = new Scanner( System.in );

	public static void main( String[] args ) {

		for(long val = scanner.nextLong(); val != -1; val = scanner.nextLong() ){
			System.out.println(isBeeenums(val) ? 'Y' : 'N');
		}
	}

	public static boolean isBeeenums(long val){
		long delta =  -12*(1-val) + 9;
		double sqrtDelta = Math.sqrt( delta );
		if(sqrtDelta != (int) sqrtDelta){
			return false;
		}
		else return (sqrtDelta - 3) % 6 == 0;
	}
}
