package solved.ENIGMATH;

import java.util.Scanner;

public class Main
{
	public static void main( String[] args ) {
		Scanner scanner = new Scanner( System.in );
		for( int T = scanner.nextInt(); T > 0; T-- ) {
			int A = scanner.nextInt();
			int B = scanner.nextInt();
			int gcd = egcd( A, B );
			System.out.println( (B / gcd) + " " + (A / gcd) );
		}
		scanner.close();
	}

	public static int egcd( int a, int b ) {
		if( a == 0 )
			return b;
		while( b != 0 ) {
			if( a > b )
				a = a - b;
			else
				b = b - a;
		}
		return a;
	}
}
