package solved.ADAFRIEN;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

public class Main
{
	public static void main( String[] args ) {
		MyScanner scanner = new MyScanner( );
		Map<String, Long> friendshipsCosts = new HashMap<>(  ) ;
		long Q = scanner.nextLong();
		long K = scanner.nextLong();
		while( Q>0 ){
			String data[] = scanner.nextLine().split( " " );
			long cost = Integer.parseInt( data[1] );
			if(friendshipsCosts.containsKey( data[0] )){
				cost += friendshipsCosts.get(data[0]);
			}
			friendshipsCosts.put( data[0], cost );
			Q--;
		}
		List<Long> costsPerFriend = new ArrayList<>( friendshipsCosts.values() );

		if(friendshipsCosts.size() > K ) {
			Collections.sort( costsPerFriend );
		}
		long totalCosts = 0;
		for(int i = 0, last = costsPerFriend.size()-1; i<K && i<=last; i++){
			totalCosts+=costsPerFriend.get( last-i );
		}
		System.out.println(totalCosts);
	}

	public static class MyScanner {
		BufferedReader br;
		StringTokenizer st;

		public MyScanner() {
			br = new BufferedReader(new InputStreamReader(System.in));
		}

		String next() {
			while (st == null || !st.hasMoreElements()) {
				try {
					st = new StringTokenizer(br.readLine());
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			return st.nextToken();
		}

		long nextLong() {
			return Long.parseLong( next());
		}

		String nextLine() {
			String str = "";
			try {
				str = br.readLine();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return str;
		}
	}

}
