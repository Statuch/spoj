package solved.LASTDIG2;

import java.util.Scanner;

public class Main {
	private static int[][] lastDigits = new int[][] { { 0 }, { 1 }, { 2, 4, 8, 6 }, { 3, 9, 7, 1 }, { 4, 6 }, { 5 },
			{ 6 }, { 7, 3, 1 }, { 8, 4, 2, 6 }, { 9, 1 } };

	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		int t = s.nextInt();
		s.nextLine();
		while (t > 0) {
			String l = s.nextLine();
			int sP = l.indexOf(' ');
			int a = l.charAt(sP - 1) - '0';
			long b = Long.parseLong(l.substring(sP + 1));
			long index = (b - 1L) % lastDigits[a].length;
			System.out.println(b == 0 ? 1 : lastDigits[a][(int) index]);
			t--;
		}
		s.close();
	}
}
