package solved.GIRLSNBS;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

import javax.print.attribute.IntegerSyntax;

public class Main {
	public static void main(String[] args) {
		MyScanner scanner = new MyScanner();
		long girls = scanner.nextLong();
		long boys = scanner.nextLong();
		while (girls != -1 && boys != -1) {
			long result = 0;
			if(girls == 0) {
				result = boys;
			} else if(boys == 0) {
				result = girls;
			} else {
				long predominan = girls > boys ? girls : boys;
				long inferiors = girls > boys ? boys : girls;
				inferiors++;
				result = (long) Math.ceil( ((double)predominan )/((double) inferiors));		
			}
			System.out.println(result);
			girls = scanner.nextLong();
			boys = scanner.nextLong();
		}
	}

	public static class MyScanner {
		BufferedReader br;
		StringTokenizer st;

		public MyScanner() {
			br = new BufferedReader(new InputStreamReader(System.in));
		}

		String next() {
			while (st == null || !st.hasMoreElements()) {
				try {
					st = new StringTokenizer(br.readLine());
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			return st.nextToken();
		}

		long nextLong() {
			return Long.parseLong(next());
		}
	}
}
