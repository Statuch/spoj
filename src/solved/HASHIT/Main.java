package solved.HASHIT;

import java.util.Scanner;

public class Main
{
	String[] keys = new String[101];

	public int find( String key ) {

		for( int j = 0; j <= 19; j++ ) {
			int index = (hash( key ) + (j * j) + (23 * j)) % 101;
			if( key.equals( keys[index] ) ) {
				return index;
			}
		}
		return -1;
}

	public void add( String key ) {
		if(find(key) == -1){
			int hash = hash( key );
			for (int j = 0; j<= 19; j++){
				int tmpIndex = (hash + (j*j) + (23*j))%101;
				if(keys[tmpIndex] == null ) {
					keys[tmpIndex] = key;
					return;
				}
			}
		}

	}

	public void delete( String key ) {
		int index = find( key );
		if( index >= 0 ) {
			keys[index] = null;
		}
	}

	public int hash( String key ) {
		int hkey = 0;
		int i = 1;
		for( char a : key.toCharArray() ) {
			hkey += a * i;
			i++;
		}
		return hkey * 19;
	}

	public int size(){
		int size = 0;
		for(String val : keys){
			if(val != null) size++;
		}
		return size;
	}
	public static void main( String[] args ) {
		Scanner scanner = new Scanner( System.in );
		for( int tests = Integer.parseInt( scanner.nextLine() ); tests > 0; tests-- ) {
			Main hashmap = new Main();
			for( int words = Integer.parseInt( scanner.nextLine() ); words > 0; words-- ) {
				String line = scanner.nextLine();
				String[] action = line.split( ":" );
				switch( action[0] ) {
					case "ADD":
						hashmap.add( action[1] );
						break;
					case "DEL":
						hashmap.delete( action[1] );
						break;
					default:
						break;
				}
			}
			System.out.println(hashmap.size());
			for( int i = 0; i < 101; i++ ) {
				if( hashmap.keys[i] != null ) {
					System.out.println( i + ":" + hashmap.keys[i] );
				}
			}
		}
	}
}
