//package solved.HASHIT;
//
//import org.junit.Assert;
//import org.junit.Test;
//
//public class Tests
//{
//
//
//	@Test
//	public void givenEmptyMapWhenAddThenOneNewElement(){
//		Main underTest = new Main();
//		underTest.add( "jasio" );
//		Assert.assertTrue( underTest.find( "jasio" ) >= 0 );
//	}
//
//
//	@Test
//	public void given2ElementsWithSameHashWhenAddAfterRemoveThenDontAddDuplicates(){
//		Main underTest = new Main();
//
//		underTest.add( "basia" );
//		underTest.add( "rafal" );
//
//		underTest.delete( "basia" );
//		underTest.add( "rafal" );
//
//		int rafals = 0;
//		for(String key : underTest.keys ){
//			Assert.assertNotEquals( "basia", key);
//			if("rafal".equals(key)){
//				rafals ++;
//			}
//		}
//		Assert.assertEquals( 1, rafals);
//	}
//}
