package solved.NOTATRI;

import java.io.IOException;
import java.util.Arrays;

public class Main {
    public static void main(String[] args) throws IOException {
        for(int numbers = readInt(); numbers != 0; numbers = readInt()){
            int[] lengths = new int[numbers];
            for(int i = 0; i<numbers; i++){
                lengths[i] = readInt();
            }
            Arrays.sort(lengths);
            int score = 0;
            for(int firstIdx = 0; firstIdx<=numbers-3; firstIdx++){
                for(int secondIdx = firstIdx+1; secondIdx<=numbers-2; secondIdx++){
                    score += positionOfFirstBiggerThan(lengths, secondIdx+1, lengths[firstIdx]+lengths[secondIdx]);
                }
            }
            System.out.println(score);
        }
    }

    private static int positionOfFirstBiggerThan(int[] array, int startIdx, int maxValue) {
        int start = startIdx, end=array.length-1;
        while(end-start>1){
            int middle = (start+end)/2;
            if(maxValue == array[middle]){
                start = middle+1;
            }
            else if(array[middle] > maxValue){
                end = middle;
            }
            else{
                start = middle+1;
            }
        }
        if(array[start] > maxValue) return array.length-start;
        else if (array[end] > maxValue) return array.length-end;
        else return 0;
    }

    private static int readInt() throws IOException {
        int ret = 0;
        boolean dig = false;
        for (int c = 0; (c = System.in.read()) != -1; ) {
            if (c >= '0' && c <= '9') {
                dig = true;
                ret = ret * 10 + c - '0';
            } else if (dig) break;
        }
        return ret;
    }
}
