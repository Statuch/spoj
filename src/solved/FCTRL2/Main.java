package solved.FCTRL2;

import java.math.BigDecimal;
import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		for (int tests = scanner.nextInt(); tests > 0; tests--) {
			int val = scanner.nextInt();
			BigDecimal result = new BigDecimal(1);
			for (int i = 1; i <= val; i++) {
				result = result.multiply(new BigDecimal(i));
			}
			System.out.println(result.toString());
		}
		scanner.close();
	}
}
