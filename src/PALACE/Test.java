package PALACE;

import java.util.List;

public class Test {
static List<String> permutations = Permutation.perm1("12345");
		
public static void main(String[] args) {
	int[] first = new int[permutations.get(0).length()];
	words: for(String value: permutations) {
		int prev = Integer.parseInt(value.substring(0,1));
		for(int i = 1; i<value.length(); i++) {
			int cur = Integer.parseInt(value.substring(i,i+1));
			if ( cur - prev == 1 || prev - cur == 1 ) {
				continue words;
			}
			prev = cur;
		}
		int f = Integer.parseInt(value.substring(0,1));
		first[f-1]++;
		//System.out.println(value);
	}
	System.out.println("------");
	for(int i : first) {
		System.out.print(i+",");

	}
	
}
}
