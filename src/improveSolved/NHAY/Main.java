package improveSolved.NHAY;

import java.io.*;

public class Main {

    private static BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    public static void main(String[] args) throws IOException {

        while (reader.readLine() != null) {
            findOccurences(reader.readLine(), reader.readLine());
        }
    }

    private static void findOccurences(String needle, String hay) {
        if(needle.length() > hay.length()) return;

        int[] kmpTemplate = new int[needle.length()];
        kmpTemplate(kmpTemplate, needle);

        int needleIdx = 0;
        int needleLastIdx = needle.length() - 1;
        int hayIdx = 0;
        int hayLastIdx = hay.length() - 1;

        boolean matchFound = false;
        while (hayIdx <= hayLastIdx) {
            if (needle.charAt(needleIdx) == hay.charAt(hayIdx)) {
                if (needleIdx == needleLastIdx) {
                    System.out.println(hayIdx - needleLastIdx);
                    needleIdx = kmpTemplate[needleIdx];
                    matchFound = true;
                } else {
                    needleIdx++;
                }
            } else if (needleIdx != 0) {
                needleIdx = kmpTemplate[needleIdx];
                continue;
            }
            hayIdx++;
        }
        if(matchFound){
            System.out.println();
        }
    }

    private static void kmpTemplate(int[] template, String needle) {
        int length = needle.length();

        int start = 0;
        int end = 1;
        while (end < length) {

            if (needle.charAt(start) == needle.charAt(end)) {
                template[end] = ++start;
                end++;
            } else if (start == 0) {
                end++;
            } else {
                start = template[start - 1];
            }
        }
    }
}
