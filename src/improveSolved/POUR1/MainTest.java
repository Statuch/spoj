package improveSolved.POUR1;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

class MainTest {

	@Test
	void test() {
		assertEquals( 20, Main.minSteps( 12, 11, 6 ) );
		assertEquals( 2, Main.minSteps( 5, 2, 3 ) );
		assertEquals( -1, Main.minSteps( 2, 3, 4 ) );
		assertEquals( 4, Main.minSteps(2, 10, 4 ) );
		assertEquals( 4, Main.minSteps(2, 10, 6 ) );
		assertEquals( -1, Main.minSteps(2, 10, 5 ) );
		assertEquals( 12, Main.minSteps(3, 11, 4 ) );
	}
}
