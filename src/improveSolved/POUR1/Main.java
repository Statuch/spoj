package improveSolved.POUR1;

import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException {
        for (int tests = readInt(); tests > 0; tests--) {
            System.out.println(minSteps(readInt(), readInt(), readInt()));
        }
    }

    public static int minSteps(int vesselA, int vesselB, int requiredAmount) {
        if(vesselA == requiredAmount || vesselB == requiredAmount) return 1;
        else if(requiredAmount > vesselA && requiredAmount > vesselB) return  -1;
        else if( requiredAmount % gcd(vesselA, vesselB) != 0 ) return -1;

        int max = vesselA >= vesselB ? vesselA : vesselB;
        int min = max == vesselA ? vesselB : vesselA;

        int gcd = gcd(vesselA, vesselB);

        int result = 0;
        for (int problemClass = 0; true; problemClass++) {
            if (problemClass * max + requiredAmount % min == 0) {
                result = (problemClass * max + requiredAmount) / min;
                result = (result + problemClass) * 2;
                break;
            }
        }

        int movesPerCycle = vesselA / gcd + vesselB / gcd - 1;
        if (min >= requiredAmount) result--;

        return (movesPerCycle - result < result) ? movesPerCycle - result : result;
    }


    public static int gcd(int a, int b) {
        if (b == 0)
            return a;
        return gcd(b, a % b);
    }

    private static int readInt() throws IOException {
        int ret = 0;
        boolean dig = false;
        for (int c = 0; (c = System.in.read()) != -1; ) {
            if (c >= '0' && c <= '9') {
                dig = true;
                ret = ret * 10 + c - '0';
            } else if (dig) break;
        }
        return ret;
    }

}
