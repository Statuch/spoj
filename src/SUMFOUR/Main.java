package SUMFOUR;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.StringTokenizer;

public class Main {
    public static void main(String[] args) {
        MyScanner scanner = new MyScanner();
        int rows = scanner.nextInt();
        var as = new int[rows];
        var bs = new int[rows];
        var cs = new int[rows];
        var ds = new int[rows];
        while(rows-- > 0){
            as[rows] = scanner.nextInt();
            bs[rows] = scanner.nextInt();
            cs[rows] = scanner.nextInt();
            ds[rows] = scanner.nextInt();
        }

        var abs = orderedJoin(as, bs);
        var cds = orderedJoin(cs, ds);

        System.out.println(countMatches(abs, cds));

    }

    private static int countMatches(int[] abs, int[] cds) {
        int result = 0;
        for(int nextAbIndex = 0, nextCdIndex = cds.length-1; nextAbIndex < abs.length && nextCdIndex >=0;){
            if(abs[nextAbIndex] == -cds[nextCdIndex]){
                int abCount = 0;
                while(abs[nextAbIndex] == abs[nextAbIndex+ ++abCount]);
                int cdCount = 0;
                while(cds[nextCdIndex] == cds[nextCdIndex - ++cdCount]);
                result+= abCount * cdCount;
                nextAbIndex+=abCount;
                nextCdIndex-=cdCount;
            }
            else if(abs[nextAbIndex] < -cds[nextCdIndex]){
                nextAbIndex++;
            }
            else{
                nextCdIndex--;
            }
        }
        return result;
    }

    private static int[] orderedJoin(int[] as, int[] bs) {
        var result = new int[as.length * bs.length];
        int index = 0;
        for(var a : as){
               for(var b: bs){
                   result[index++] = a+b;
               }
           }
        Arrays.sort(result);
        return result;
    }

    public static class MyScanner {
        BufferedReader br;
        StringTokenizer st;

        public MyScanner() {
            br = new BufferedReader(new InputStreamReader(System.in));
        }

        String next() {
            while (st == null || !st.hasMoreElements()) {
                try {
                    st = new StringTokenizer(br.readLine());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return st.nextToken();
        }

        int nextInt() {
            return Integer.parseInt(next());
        }

        String nextLine() {
            String str = "";
            try {
                str = br.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return str;
        }
    }
}
