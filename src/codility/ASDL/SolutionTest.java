package codility.ASDL;

import org.junit.Assert;
import org.junit.Test;

public class SolutionTest
{
	public Solution solution = new Solution();
	@Test
	public void test(){

		Assert.assertEquals(1, solution.solution( "L", 1 ));
		Assert.assertEquals(0, solution.solution( "M", 1 ));
		Assert.assertEquals(1, solution.solution( "M", 0 ));
		Assert.assertEquals(0, solution.solution( "L", 0 ));

		Assert.assertEquals(1, solution.solution( "MLMMLLM", 3 ));
		Assert.assertEquals(2, solution.solution( "MLMMMLMMMM", 2 ));



		Assert.assertEquals(0, solution.solution( "LLLMMMLLL", 3 ));
		Assert.assertEquals(1, solution.solution( "LMMMLMML", 2 ));

		Assert.assertEquals(1, solution.solution( "LMMML", 2 ));
		Assert.assertEquals(1, solution.solution( "LMMMML", 2 ));
		Assert.assertEquals(1, solution.solution( "LMMMMML", 2 ));
		Assert.assertEquals(2, solution.solution( "LMMMMMML", 2 ));

		Assert.assertEquals(2, solution.solution( "LLMMMLL", 5 ));
		Assert.assertEquals(2, solution.solution( "LLMMMLLMMMLL", 5 ));

		Assert.assertEquals(2, solution.solution( "LMLMLMLMLML", 5 ));

		Assert.assertEquals(5, solution.solution( "LLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL", 5 ));
		Assert.assertEquals(9, solution.solution( "MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM", 10 ));


		Assert.assertEquals(3, solution.solution( "LMMMLMMMMMLMMM", 2 ));


	}
}
