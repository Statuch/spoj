package codility.ASDL;

public class Solution
{
	public int solution( String input, final int K ) {

		char[] inputArray = input.toCharArray();

		int result = K == 0 ? 0 :Integer.MAX_VALUE;
		int cuts = -1;
		int debt = 0;
		int tmpValue = 0;

		for( int start = -1, end = 0, last = inputArray.length-1; start < last; ) {
			char nextModul = inputArray[++start];
			if(K == 0){
				result += nextModul == 'M'  ? 1 : 0;
				continue;
			}
			tmpValue++;
			if( nextModul == 'L' )
				debt++;

			if( K == tmpValue ) {
				if( debt == 0 ) {
					cuts = cuts < 0 ? 0 : cuts;
					if( start < last && inputArray[start+1] == 'M' ) {
						end = start+2;
						start++;
						cuts++;
						tmpValue = 0;
						debt = 0;
						continue;
					}
					else{
						end++;
					}
				} else {
					int tmpResult = debt;
					if(end>0 && inputArray[end-1] == 'M') tmpResult++;
					if(start<last && inputArray[start+1] == 'M') tmpResult++;
					if(tmpResult<result) result = tmpResult;
					debt -= inputArray[end] == 'L' ? 1 : 0;
					end++;
				}
				tmpValue--;
			}
		}
		return cuts<0 ? result : cuts;
	}
}
