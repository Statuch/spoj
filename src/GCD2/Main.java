package GCD2;

import java.math.BigInteger;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int tests = scanner.nextInt();
        for (scanner.nextLine(); tests > 0; tests--) {
            String line[] = scanner.nextLine().split(" ");
            System.out.println(gcd(new BigInteger(line[0]), new BigInteger(line[1])).toString());
        }
        scanner.close();
    }

    static BigInteger gcd(BigInteger a, BigInteger b) {
        if (b.equals(BigInteger.ZERO)) {
            return a;
        }
        return gcd(b, a.mod(b));
    }
}
